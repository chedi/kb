from suit_redactor.widgets          import RedactorWidget
from django.contrib.flatpages.admin import FlatpageForm


class FlatpageForm(FlatpageForm):
    class Meta:
        widgets = {
            'content': RedactorWidget(editor_options={'lang': 'fr', 'minHeight': 300}),
        }
