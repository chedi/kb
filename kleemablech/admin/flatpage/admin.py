from django.utils.translation       import gettext_lazy as _
from django.contrib.flatpages.admin import FlatPageAdmin

from kleemablech.admin.flatpage.forms import FlatpageForm


class FlatPageAdmin(FlatPageAdmin):
    form      = FlatpageForm
    fieldsets = [
        (_('Page Informations'), {
            'fields' : ['url', 'title', 'sites', ]
        }),
        (_('Page Content'), {
            'fields' : ['content', ]
        }),
    ]
