from django.forms import ModelForm

from kleemablech.models import NewsletterEmails


class NewsletterEmailsForm(ModelForm):
    class Meta:
        model  = NewsletterEmails
        fields = '__all__'
