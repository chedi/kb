from reversion.admin import VersionAdmin

from kleemablech.admin.news_letter.forms import NewsletterEmailsForm


class NewsletterEmailsAdmin(VersionAdmin):
    form         = NewsletterEmailsForm
    list_display = ['email', 'active']
