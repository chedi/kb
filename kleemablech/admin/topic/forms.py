from django.forms import ModelForm

from kleemablech.models.faq import Topic


class TopicForm(ModelForm):
    class Meta:
        model  = Topic
        fields = '__all__'
