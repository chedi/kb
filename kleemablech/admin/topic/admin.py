from suit.admin      import SortableModelAdmin
from reversion.admin import VersionAdmin

from kleemablech.admin.topic.forms import TopicForm


class TopicAdmin(VersionAdmin, SortableModelAdmin):
    form     = TopicForm
    sortable = 'order'
