from reversion.admin import VersionAdmin

from kleemablech.admin.win.forms import WinForm
from kleemablech.admin.win.forms import CampaignWinForm


class WinAdmin(VersionAdmin):
    form         = WinForm
    list_filter  = ('user', 'date', 'amount')
    list_display = ('user', 'date', 'amount')


class CampaignWinAdmin(VersionAdmin):
    form         = CampaignWinForm
    list_filter  = ('user', 'campaign', 'date', 'amount', 'verified')
    list_display = ('date', 'user', 'campaign', 'amount', 'verified')
