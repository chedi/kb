from django.forms import ModelForm

from kleemablech.models import Win
from kleemablech.models import CampaignWin


class WinForm(ModelForm):
    class Meta:
        model   = Win
        fields  = '__all__'


class CampaignWinForm(ModelForm):
    class Meta:
        model   = CampaignWin
        fields  = '__all__'
