from django.contrib.admin            import site
from django.contrib.flatpages.models import FlatPage

from kleemablech.models import Win
from kleemablech.models import User
from kleemablech.models import Topic
from kleemablech.models import Refill
from kleemablech.models import Campaign
from kleemablech.models import Operator
from kleemablech.models import Question
from kleemablech.models import CampaignWin
from kleemablech.models import PhoneNumber
from kleemablech.models import CampaignView
from kleemablech.models import FeaturedTopic
from kleemablech.models import FacebookShare
from kleemablech.models import NewsletterEmails
from kleemablech.models import UserFacebookShare

from kleemablech.admin.win.admin            import WinAdmin
from kleemablech.admin.win.admin            import CampaignWinAdmin
from kleemablech.admin.user.admin           import UserAdmin
from kleemablech.admin.share.admin          import FacebookShareAdmin
from kleemablech.admin.share.admin          import UserFacebookShareAdmin
from kleemablech.admin.topic.admin          import TopicAdmin
from kleemablech.admin.refill.admin         import RefillAdmin
from kleemablech.admin.question.admin       import QuestionAdmin
from kleemablech.admin.flatpage.admin       import FlatPageAdmin
from kleemablech.admin.campaign.admin       import CampaignAdmin
from kleemablech.admin.operator.admin       import OperatorAdmin
from kleemablech.admin.news_letter.admin    import NewsletterEmailsAdmin
from kleemablech.admin.phone_number.admin   import PhoneNumberAdmin
from kleemablech.admin.campaign_view.admin  import CampaignViewAdmin
from kleemablech.admin.featured_topic.admin import FeaturedTopicAdmin

site.unregister(FlatPage)

site.register(Win              , WinAdmin              )
site.register(User             , UserAdmin             )
site.register(Topic            , TopicAdmin            )
site.register(Refill           , RefillAdmin           )
site.register(Question         , QuestionAdmin         )
site.register(Operator         , OperatorAdmin         )
site.register(Campaign         , CampaignAdmin         )
site.register(FlatPage         , FlatPageAdmin         )
site.register(CampaignWin      , CampaignWinAdmin      )
site.register(PhoneNumber      , PhoneNumberAdmin      )
site.register(CampaignView     , CampaignViewAdmin     )
site.register(FeaturedTopic    , FeaturedTopicAdmin    )
site.register(FacebookShare    , FacebookShareAdmin    )
site.register(NewsletterEmails , NewsletterEmailsAdmin )
site.register(UserFacebookShare, UserFacebookShareAdmin)
