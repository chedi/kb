from django.forms import ModelForm
from django.forms import Textarea
from django.forms import TextInput
from django.forms import URLInput

from suit.widgets import SuitSplitDateTimeWidget

from kleemablech.models import FacebookShare
from kleemablech.models import UserFacebookShare


class FacebookShareForm(ModelForm):
    class Meta:
        model   = FacebookShare
        fields  = '__all__'
        widgets = {
            'name'   : TextInput(attrs={'class': 'input-xxlarge', }),
            'link'   : URLInput (attrs={'class': 'input-xxlarge', }),
            'message': Textarea (attrs={'class': 'input-xxlarge', 'rows': 3}),
            'caption': Textarea (attrs={'class': 'input-xxlarge', 'rows': 3}),
        }


class UserFacebookShareForm(ModelForm):
    class Meta:
        model   = UserFacebookShare
        fields  = '__all__'
        widgets = {
            'date': SuitSplitDateTimeWidget,
        }
