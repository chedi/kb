from django.utils.translation import ugettext_lazy as _

from reversion.admin import VersionAdmin

from kleemablech.admin.share.forms import FacebookShareForm
from kleemablech.admin.share.forms import UserFacebookShareForm


class FacebookShareAdmin(VersionAdmin):
    form            = FacebookShareForm
    list_display    = ('date', 'name', 'picture_img', 'active')
    readonly_fields = ('picture_img',)

    fieldsets     = [
        (_('Share Information'), {
            'fields' : ['name', 'link', 'message', 'caption', ]
        }),

        (_('Share Media'), {
            'fields' : ['picture', 'picture_img', ]
        }),

        (_('Share State'), {
            'fields' : ['active', ]
        })
    ]


class UserFacebookShareAdmin(VersionAdmin):
    form         = UserFacebookShareForm
    list_display = ('date', 'user', 'share')

    fieldsets = [
        (_('Share Information'), {
            'fields' : ['date', 'user', 'share', ]
        }),
    ]
