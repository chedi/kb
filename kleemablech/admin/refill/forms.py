from django.forms import ModelForm

from suit.widgets import SuitSplitDateTimeWidget

from kleemablech.models import Refill


class RefillForm(ModelForm):
    class Meta:
        model   = Refill
        fields  = '__all__'
        widgets = {
            'date': SuitSplitDateTimeWidget
        }
