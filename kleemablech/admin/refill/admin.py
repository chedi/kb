from reversion.admin import VersionAdmin

from kleemablech.admin.refill.forms import RefillForm


class RefillAdmin(VersionAdmin):
    form = RefillForm
