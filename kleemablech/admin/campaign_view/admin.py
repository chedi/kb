from reversion.admin import VersionAdmin

from kleemablech.admin.campaign_view.forms import CampaignViewForm


class CampaignViewAdmin(VersionAdmin):
    form         = CampaignViewForm
    list_filter  = ('user', 'date', 'campaign')
    list_display = ('user', 'date', 'campaign')
