from django.forms import ModelForm

from kleemablech.models.campaign import CampaignView


class CampaignViewForm(ModelForm):
    class Meta:
        model  = CampaignView
        fields = '__all__'
