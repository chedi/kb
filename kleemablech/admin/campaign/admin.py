from reversion.admin          import VersionAdmin
from django.utils.translation import ugettext_lazy as _

from kleemablech.admin.campaign.forms import CampaignForm


class CampaignAdmin(VersionAdmin):
    form            = CampaignForm
    list_display    = ('title', 'brand', 'start_date', 'image_img', 'active', 'position')
    search_fields   = ('title', 'brand', )
    list_filter     = ('brand', 'start_date', 'active', 'position')
    readonly_fields = ('image_img', 'video_video')

    fieldsets     = [
        (_('Campaign Information'), {
            'classes': ['suit-tab suit-tab-informations', ],
            'fields' : ['title', 'brand', 'duration', 'position', 'start_date', 'active']
        }),
        (_('Campaign Earnings'), {
            'classes': ['suit-tab suit-tab-earnings', ],
            'fields' : ['draw'                     ,
                        'draw_amount'              ,
                        'total_winners'            ,
                        'total_amount'             ,
                        'unique_visitors'          ,
                        'direct_win_amount'        ,
                        'cumulative_win_amount'    ,
                        'direct_win_percentage'    ,
                        'campaign_end_draw_amount' ,
                        'cumulative_win_percentage']
        }),
        (_('Campaign Media'), {
            'classes': ['suit-tab suit-tab-media', ],
            'fields' : ['image', 'image_img', 'video', 'video_video']
        }),
    ]
    suit_form_tabs = (
        ('informations', _('Campaign Information')),
        ('earnings'    , _('Campaign Earnings'   )),
        ('media'       , _('Campaign Media'      )),
    )
