from django.forms import ModelForm
from suit.widgets import SuitDateWidget

from kleemablech.models import Campaign


class CampaignForm(ModelForm):
    class Meta:
        model   = Campaign
        fields  = '__all__'
        widgets = {
            'start_date': SuitDateWidget,
        }
