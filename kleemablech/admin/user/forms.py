from django.forms import ModelForm

from suit.widgets import SuitDateWidget
from suit.widgets import SuitSplitDateTimeWidget

from kleemablech.models import User


class UserForm(ModelForm):
    class Meta:
        model   = User
        fields  = '__all__'
        widgets = {
            'birthday'   : SuitDateWidget,
            'last_login' : SuitSplitDateTimeWidget,
            'date_joined': SuitSplitDateTimeWidget,
        }
