from reversion.admin           import VersionAdmin
from image_cropping            import ImageCroppingMixin
from django.utils.translation  import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin

from kleemablech.admin.user.forms         import UserForm
from kleemablech.admin.phone_number.admin import PhoneNumberAdminInline


class UserAdmin(ImageCroppingMixin, AuthUserAdmin, VersionAdmin):
    form          = UserForm
    list_display  = ('email', 'gender', 'region',)
    search_fields = ('email', )

    fieldsets = [
        (_('User Informations'), {
            'fields' : [
                'email', 'first_name', 'last_name',
                'gender', 'region', 'birthday',
                'picture', 'cropping', 'is_active',
                'date_joined', 'last_login'
            ]
        }),
    ]

    add_fieldsets = (
        (_('User Informations'), {
            'classes': ('wide',),
            'fields' : [
                'email', 'first_name', 'last_name',
                'password1', 'password2', 'gender', 'region',
                'picture', 'cropping', 'birthday'
            ]
        }),)

    inlines = [PhoneNumberAdminInline, ]
