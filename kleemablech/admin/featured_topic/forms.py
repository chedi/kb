from django.forms import ModelForm

from kleemablech.models import FeaturedTopic


class FeaturedTopicForm(ModelForm):
    class Meta:
        model   = FeaturedTopic
        fields  = '__all__'
