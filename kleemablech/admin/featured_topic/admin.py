from suit.admin      import SortableModelAdmin
from reversion.admin import VersionAdmin

from kleemablech.admin.featured_topic.forms import FeaturedTopicForm


class FeaturedTopicAdmin(VersionAdmin, SortableModelAdmin):
    form         = FeaturedTopicForm
    sortable     = 'order'
    list_display = ['text', 'topic']
