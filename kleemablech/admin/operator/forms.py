from django.forms import ModelForm

from kleemablech.models import Operator


class OperatorForm(ModelForm):
    class Meta:
        model   = Operator
        fields  = '__all__'
