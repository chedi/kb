from reversion.admin import VersionAdmin

from django.utils.translation import gettext_lazy as _

from kleemablech.admin.operator.forms import OperatorForm


class OperatorAdmin(VersionAdmin):
    form         = OperatorForm
    list_display = ('name', 'device')

    fieldsets = [
        (_('Operator informations'), {
            'fields' : [
                'name', 'device', 'prefixes',
                'ussd_string', 'device_baud_rate', 'requires_reply',
            ]
        }),
    ]
