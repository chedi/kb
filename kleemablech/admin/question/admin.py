from suit.admin      import SortableModelAdmin
from reversion.admin import VersionAdmin

from kleemablech.admin.question.forms import QuestionForm


class QuestionAdmin(VersionAdmin, SortableModelAdmin):
    form     = QuestionForm
    sortable = 'order'
