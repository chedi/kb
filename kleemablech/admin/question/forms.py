from django.forms          import ModelForm

from suit_redactor.widgets import RedactorWidget

from kleemablech.models.faq import Question


class QuestionForm(ModelForm):
    class Meta:
        model   = Question
        fields  = '__all__'
        widgets = {
            'answer': RedactorWidget(editor_options={'lang': 'fr', 'minHeight': 300}),
        }
