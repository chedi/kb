from django.forms import ModelForm

from kleemablech.models import PhoneNumber


class PhoneNumberForm(ModelForm):
    class Meta:
        model   = PhoneNumber
        fields  = '__all__'
