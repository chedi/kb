from reversion.admin      import VersionAdmin
from django.contrib.admin import StackedInline

from kleemablech.models                   import PhoneNumber
from kleemablech.admin.phone_number.forms import PhoneNumberForm


class PhoneNumberAdminInline(StackedInline):
    model = PhoneNumber
    extra = 1


class PhoneNumberAdmin(VersionAdmin):
    form         = PhoneNumberForm
    list_display = ('number', 'user')
