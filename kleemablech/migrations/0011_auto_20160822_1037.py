# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-08-22 09:37
from __future__ import unicode_literals

from django.db import migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('kleemablech', '0010_operator_device_baud_rate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='picture',
            field=image_cropping.fields.ImageCropField(default='profile/default.png', upload_to='profile', verbose_name='picture'),
        ),
    ]
