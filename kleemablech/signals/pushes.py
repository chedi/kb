from pusher                import Pusher
from constance             import config
from easy_thumbnails.files import get_thumbnailer

from kleemablech.utils.strings import decimal_to_string


def publish_last_winners_list(sender, instance, **kwargs):
    pusher_client = Pusher(key    = config.PUSHER_KEY   ,
                           app_id = config.PUSHER_APP_ID,
                           secret = config.PUSHER_SECRET,
                           cluster= 'eu'                ,
                           ssl    = True)

    thumbnail_url = get_thumbnailer(instance.user.picture).get_thumbnail({
        'size'  : (100, 100),
        'box'   : instance.user.cropping,
        'crop'  : True,
        'detail': True,
    }).url

    pusher_client.trigger('winners_channel', 'update_event', {
        'win_amount'    : decimal_to_string(instance.amount),
        'winner_name'   : instance.user.get_full_name(),
        'winner_region' : instance.user.region,
        'winner_picture': thumbnail_url,
    })
