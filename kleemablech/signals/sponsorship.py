from datetime         import datetime
from constance        import config
from django.db.models import Q

from invitations.models import Invitation

from kleemablech.models import Win
from kleemablech.models import SponsorshipWin


def sponsorship_wins_after_signup(sender, request, user, **kwargs):
    invitation = Invitation.objects.filter(email=user.email).filter(accepted=True).first()

    if invitation:
        sponsorship_win = SponsorshipWin.objects.create(
            user   = invitation.inviter,
            state  = 'Waiting',
            amount = config.SPONSORSHIP_AMOUNT)

        total_wins_parrainage = SponsorshipWin.objects.filter(
            Q(state='Payed'                     ) &
            Q(user=invitation.inviter           ) &
            Q(date__year=datetime.today().year  ) &
            Q(date__month=datetime.today().month))

        total_wins_inviter = Win.total_win_user(invitation.inviter)

        if total_wins_parrainage.count() < 5 and total_wins_inviter < 0.5:
            Win.objects.create(
                date   = sponsorship_win.date_win,
                user   = invitation.inviter,
                amount = sponsorship_win.amount)

            sponsorship_win.state = 'Payed'
            sponsorship_win.save()

    Win.objects.create(user=user, amount=config.SPONSORSHIP_AMOUNT)


def sponsorship_wins_after_email_confirmation(sender, email_address, **kwargs):
    Win.objects.create(user=email_address.user, amount=config.SPONSORSHIP_AMOUNT)
