from django.db.models.signals import post_save

from allauth.account.signals import user_signed_up

from kleemablech.models              import Win
from kleemablech.models              import CampaignWin
from kleemablech.signals.wins        import check_campaign_end_on_win
from kleemablech.signals.pushes      import publish_last_winners_list
from kleemablech.signals.sponsorship import sponsorship_wins_after_signup


def setup_signals():
    user_signed_up.connect(sponsorship_wins_after_signup)

    post_save.connect(publish_last_winners_list, sender=Win        )
    post_save.connect(check_campaign_end_on_win, sender=CampaignWin)
