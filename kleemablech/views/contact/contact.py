from django.http               import JsonResponse
from django.utils.translation  import gettext as _
from django.views.generic.edit import FormView

from kleemablech.forms import ContactForm


class ContactView(FormView):
    form_class    = ContactForm
    template_name = 'pages/contact.html'

    def get_form_kwargs(self):
        form_kwargs = super(ContactView, self).get_form_kwargs()
        form_kwargs.update({'user': self.request.user})
        return form_kwargs

    def form_invalid(self, form):
        response = super(ContactView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(
                {'form_errors': form.errors},
                status=400)
        return response

    def form_valid(self, form):
        form.send_email()
        if self.request.is_ajax:
            return JsonResponse(
                {'status': _("The contact form has been succesfuly sent")},
                status=200)
        self.render_to_response(self.get_context_data(form=form))
