from django.shortcuts           import redirect
from django.core.urlresolvers   import reverse
from django.views.generic.edit  import FormView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.forms  import Step2Form
from kleemablech.models import PhoneNumber


class Step2SignupView(LoginRequiredMixin, FormView):
    form_class    = Step2Form
    template_name = 'pages/step2.html'

    def get(self, request, *args, **kwargs):
        if request.user.phonenumber_set.count() > 0:
            return redirect(reverse('home'))
        return super(Step2SignupView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        PhoneNumber.objects.create(
            user   = self.request.user,
            number = form.cleaned_data['phone'])
        return redirect(reverse('home'))
