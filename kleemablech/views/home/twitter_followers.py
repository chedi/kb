from redis      import Redis
from redis      import ConnectionPool
from tweepy     import API
from tweepy     import OAuthHandler
from datetime   import datetime
from constance  import config
from redis_lock import Lock

from django.http          import JsonResponse
from django.views.generic import View

TWITTER_UPDATE_INTERVAL = 600
TWITTER_FOLLOWERS_COUNT = "twitter_followers_count"
TWITTER_FOLLOWERS_CACHE = "twitter_followers_cache_last_update"


def get_twitter_followers():
    auth = OAuthHandler(config.TWITTER_CONSUMER_KEY, config.TWITTER_CONSUMER_SECRET)
    auth.set_access_token(config.TWITTER_ACCESS_TOKEN, config.TWITTER_ACCESS_TOKEN_SECRET)
    return API(auth).followers()


class TwitterFollowersCountView(View):
    def get(self, request, *args, **kwargs):
        redis_conn = Redis(connection_pool=ConnectionPool(host='127.0.0.1', port=6379, db=0))

        if redis_conn.get(TWITTER_FOLLOWERS_COUNT) is None:
            redis_conn.set(TWITTER_FOLLOWERS_COUNT, 0)
            redis_conn.set(TWITTER_FOLLOWERS_CACHE, datetime.now().strftime('%c'))

        try:
            last_update     = redis_conn.get(TWITTER_FOLLOWERS_CACHE)
            last_update     = datetime.strptime(last_update.decode('utf8'), "%c")
            update_interval = datetime.now() - last_update

            if update_interval.seconds > TWITTER_UPDATE_INTERVAL:
                lock = Lock(redis_conn, TWITTER_FOLLOWERS_CACHE)
                if lock.acquire(blocking=False):
                    redis_conn.set(TWITTER_FOLLOWERS_CACHE, datetime.now().strftime("%c"))
                    redis_conn.set(TWITTER_FOLLOWERS_COUNT, len(get_twitter_followers()))
                    lock.release()
        except Exception as e:
            pass
        followers_count = int(redis_conn.get(TWITTER_FOLLOWERS_COUNT))

        return JsonResponse({'follower_count': followers_count}, status=200)
