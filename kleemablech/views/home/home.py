from decimal import Decimal

from django.views.generic import TemplateView

from kleemablech.models import Win
from kleemablech.models import User
from kleemablech.models import Refill
from kleemablech.models import Campaign
from kleemablech.models import CampaignView
from kleemablech.models import FeaturedTopic


class HomeView(TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        user             = self.request.user
        context          = super(HomeView, self).get_context_data(**kwargs)
        active_campaigns = Campaign.objects.filter(active=True)

        context.update({
            'last_wins'           : Win.objects.all().order_by('-id')[:20],
            'campaigns'           : active_campaigns.filter(position='slider')[:4],
            'winners_total'       : Win.winners_total(),
            'wining_videos'       : active_campaigns.count(),
            'drawing_videos'      : active_campaigns.filter(position='drawing'),
            'featured_topics'     : FeaturedTopic.objects.all(),
            'total_amount_sum'    : Campaign.total_amount_sum(),
            'subscribers_count'   : User.objects.all().count(),
            'subsidized_videos'   : active_campaigns.filter(position='subsidized'),
            'winable_percentage'  : Campaign.total_winable_percentage(),
            'total_winable_amount': Campaign.total_winable_amount(),
        })

        if self.request.user.is_authenticated():
            context.update({
                'user_total_wins'                      : Win.user_total_wins(user),
                'user_total_refill'                    : Decimal(Refill.user_total_refill(user)),
                'user_total_campaigns_views'           : CampaignView.user_total_campaigns_views(user),
                'user_cumulation_remainings'           : Win.user_cumulation_remainings(user),
                'user_total_cumulated_credit'          : Win.user_total_cumulated_credit(user),
                'user_cumulation_remainings_percentage': Win.user_cumulation_remainings_percentage(user),
            })

        return context
