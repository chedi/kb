from django.http               import JsonResponse
from django.utils.translation  import gettext as _
from django.views.generic.edit import FormView

from kleemablech.forms import NewsletterEmailsForm


class NewsletterEmailsView(FormView):
    form_class = NewsletterEmailsForm

    def form_invalid(self, form):
        return JsonResponse(
            {'form_errors': form.errors},
            status=400)

    def form_valid(self, form):
        form.save()
        return JsonResponse(
            {'status': _("Your have been subscribed to the kleemablech newsletter")},
            status=200)
