from redis        import Redis
from redis        import ConnectionPool
from requests     import get
from datetime     import datetime
from constance    import config
from redis_lock   import Lock
from urllib.parse import urlencode
from urllib.parse import parse_qs

from django.http          import JsonResponse
from django.views.generic import View

FACEBOOK_UPDATE_INTERVAL = 600
FACEBOOK_FOLLOWERS_COUNT = "facebook_followers_count"
FACEBOOK_FOLLOWERS_CACHE = "facebook_followers_cache_last_update"


def get_facebook_followers():
    oauth_args = {
        'client_id'    : config.FACEBOOK_APP_ID    ,
        'grant_type'   : 'client_credentials'      ,
        'client_secret': config.FACEBOOK_APP_SECRET,
    }
    response = get('https://graph.facebook.com/oauth/access_token?{}'.format(urlencode(oauth_args)))
    response = get("https://graph.facebook.com/v2.6/{}/?fields=fan_count&access_token={}".format(
        config.FACEBOOK_PAGE_ID,
        parse_qs(response.text)['access_token'][0],
    ))
    return response.json()['fan_count']


class FacebookFollowersCountView(View):
    def get(self, request, *args, **kwargs):
        redis_conn = Redis(connection_pool=ConnectionPool(host='127.0.0.1', port=6379, db=0))

        if redis_conn.get(FACEBOOK_FOLLOWERS_COUNT) is None:
            redis_conn.set(FACEBOOK_FOLLOWERS_COUNT, 0)
            redis_conn.set(FACEBOOK_FOLLOWERS_CACHE, datetime.now().strftime('%c'))

        try:
            last_update     = redis_conn.get(FACEBOOK_FOLLOWERS_CACHE)
            last_update     = datetime.strptime(last_update.decode('utf8'), "%c")
            update_interval = datetime.now() - last_update

            if update_interval.seconds > FACEBOOK_UPDATE_INTERVAL:
                lock = Lock(redis_conn, FACEBOOK_FOLLOWERS_CACHE)
                if lock.acquire(blocking=False):
                    redis_conn.set(FACEBOOK_FOLLOWERS_CACHE, datetime.now().strftime("%c"))
                    redis_conn.set(FACEBOOK_FOLLOWERS_COUNT, get_facebook_followers())
                    lock.release()
        except Exception as e:
            pass
        followers_count = int(redis_conn.get(FACEBOOK_FOLLOWERS_COUNT))

        return JsonResponse({'follower_count': followers_count}, status=200)
