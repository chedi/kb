from django.views.generic       import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class VideoTimeoutView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/video_timeout.html'
