from django.db.models           import Q
from django.views.generic       import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models import Campaign
from kleemablech.models import CampaignWin


class VideoInformationsView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/video_information.html'

    def get_context_data(self, video, **kwargs):
        user           = self.request.user
        context        = super(VideoInformationsView, self).get_context_data(**kwargs)
        campaign       = Campaign.objects.get(id=video)
        already_viewed = CampaignWin.objects.filter(
            Q(user     = user    ) &
            Q(verified = True    ) &
            Q(campaign = campaign)
        ).exists()

        context.update({
            'campaign'      : campaign,
            'already_viewed': already_viewed,
        })

        return context
