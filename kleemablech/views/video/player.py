from subprocess import PIPE
from subprocess import Popen

from django.views.generic       import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models import Campaign


class VideoPlayerView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/video_player.html'

    def get_context_data(self, video, **kwargs):
        context  = super(VideoPlayerView, self).get_context_data(**kwargs)
        campaign = Campaign.objects.get(id=video)

        process = Popen(
            'ffprobe -v error -show_entries stream=width,height -of default=noprint_wrappers=1 ' +
            campaign.video.path, stdout=PIPE, shell=True)
        out, err = process.communicate()

        video_data = dict([_.split('=') for _ in out.decode('utf8').split()])

        context.update({
            'campaign'    : campaign,
            'video_width' : video_data['width' ],
            'video_height': video_data['height'],
        })

        return context
