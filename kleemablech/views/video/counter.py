from random   import choice
from datetime import datetime

from django.http                import HttpResponseRedirect
from django.db.models           import Q
from django.shortcuts           import render
from django.utils.crypto        import get_random_string
from django.views.generic       import View
from django.core.urlresolvers   import reverse
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models import Win
from kleemablech.models import Campaign
from kleemablech.models import CampaignWin
from kleemablech.models import CampaignView


class VideoCounterView(LoginRequiredMixin, View):
    def get(self, request, video, *args, **kwargs):
        user           = request.user
        date           = datetime.now()
        code           = get_random_string(length=32)
        campaign       = Campaign.objects.get(id=video)
        already_viewed = CampaignWin.objects.filter(
            Q(user     = user    ) &
            Q(verified = True    ) &
            Q(campaign = campaign)
        ).exists()

        CampaignView.objects.create(user     = user,
                                    date     = date,
                                    campaign = campaign)

        if not already_viewed:
            amount        = campaign.direct_win_amount
            total_winners = campaign.total_winners

            if campaign.position == 'slider':
                winners = CampaignWin.objects.filter(
                    Q(verified = True    ) &
                    Q(campaign = campaign))

                direct_winners        = winners.filter(amount=campaign.direct_win_amount    ).count()
                cumulative_winners    = winners.filter(amount=campaign.cumulative_win_amount).count()
                direct_percentage     = (direct_winners     / total_winners) * 100
                cumulative_percentage = (cumulative_winners / total_winners) * 100

                if direct_percentage >= campaign.direct_win_percentage:
                    amount = campaign.cumulative_win_amount
                elif cumulative_percentage >= campaign.cumulative_win_percentage:
                    amount = campaign.direct_win_amount
                else:
                    amount = choice([
                        campaign.direct_win_amount    ,
                        campaign.cumulative_win_amount,
                    ])

            CampaignWin.objects.create(user              = user,
                                       date              = date,
                                       amount            = amount,
                                       campaign          = campaign,
                                       verification_code = code)

        return render(request, 'pages/video_counter.html', context={
            'campaign'         : campaign,
            'already_viewed'   : already_viewed,
            'verification_code': code})

    def post(self, request, *args, **kwargs):
        code         = request.POST.get('verification_code')
        campaign_win = CampaignWin.objects.filter(verification_code=code).first()

        if campaign_win:
            if not campaign_win.verified:
                campaign_win.verified = True
                campaign_win.save()

                Win.objects.create(date   = campaign_win.date,
                                   user   = campaign_win.user,
                                   amount = campaign_win.amount)

                return HttpResponseRedirect(
                    reverse('video_felicitation', args=[campaign_win.verification_code]))
