from django.views.generic       import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models import CampaignWin


class VideoFelicitationView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/felicitations.html'

    def get_context_data(self, key, **kwargs):
        context      = super(VideoFelicitationView, self).get_context_data(**kwargs)
        campaing_win = CampaignWin.objects.filter(verification_code=key).first()

        context.update({
            'campaign'  : campaing_win.campaign,
            'win_amount': campaing_win.amount
        })

        return context
