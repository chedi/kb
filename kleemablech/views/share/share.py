from datetime  import datetime
from operator  import itemgetter
from constance import config

from django.http                import JsonResponse
from django.db.models           import Q
from django.db.models           import Count
from django.views.generic       import View
from django.utils.translation   import ugettext as _
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models import Win
from kleemablech.models import Refill
from kleemablech.models import FacebookShare
from kleemablech.models import UserFacebookShare


class ShareView(LoginRequiredMixin, View):
    def handle_no_permission(self):
        return JsonResponse(
            {'form_errors': {'__all__': [_('Only available for authenticated users'), ]}},
            status=400)

    def get_allowed_shares(self, user, date):
        user_shares = UserFacebookShare.objects.filter(Q(user=user) & Q(date__date=date.date()))

        return FacebookShare.objects.filter(active=True).exclude(
            id__in=list(map(itemgetter('share__id'),
                            filter(lambda _: _['count'] >= config.MAX_DAYLY_SHARES,
                                   user_shares.values("share__id").annotate(count=Count("pk"))))))

    def get(self, request, *args, **kwargs):
        allowed_shares = self.get_allowed_shares(request.user, datetime.now())

        if len(allowed_shares) > 0:
            share = allowed_shares[0]
            return JsonResponse({
                'form_errors'       : None,
                'share_informations': {
                    'id'     : share.id     ,
                    'name'   : share.name   ,
                    'link'   : share.link   ,
                    'caption': share.caption,
                    'message': share.message,
                    'picture': request.build_absolute_uri(share.picture.url),
                }}, status=200)
        else:
            return JsonResponse(
                {'form_errors': {'__all__': [_('Not available shares for the moment'), ]}},
                status=400)

    def post(self, request, *args, **kwargs):
        user            = request.user
        date            = datetime.now()
        allowed_shares  = self.get_allowed_shares(user, date)
        requested_share = allowed_shares.filter(id=int(request.POST['share_id'][0]))

        if len(requested_share) == 1:
            current_credit = Win.user_total_wins(user) - Refill.user_total_refill(user)
            if current_credit >= config.MINIMAL_SHARE_CREDIT:
                UserFacebookShare.objects.create(
                    user = user,
                    date = datetime.now(),
                    share= requested_share[0])
                return JsonResponse({'form_errors': None}, status=200)
            else:
                return JsonResponse(
                    {'form_errors': {'__all__': [_('You need at least 500 mil to share and win refills'), ]}},
                    status=400)
        else:
            return JsonResponse(
                {'form_errors': {'__all__': [_('You have consumed your daily sharing quota'), ]}},
                status=400)
