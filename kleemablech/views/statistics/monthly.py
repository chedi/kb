from django.http                import JsonResponse
from django.views.generic       import View
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models import Win
from kleemablech.models import Refill
from kleemablech.models import CampaignView


class MonthlyViewsStatsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return JsonResponse(CampaignView.monthly_user_stats(request.user))


class MonthlyWinsStatsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return JsonResponse(Win.monthly_user_stats(request.user))


class MonthlyRefillsStatsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return JsonResponse(Refill.monthly_user_stats(request.user))
