from datetime  import datetime
from datetime  import timedelta
from constance import config

from django.http                import JsonResponse
from django.db.models           import Q
from django.utils.translation   import ugettext as _
from django.views.generic.edit  import FormView
from django.contrib.auth.mixins import LoginRequiredMixin

from invitations.models import Invitation

from kleemablech.forms  import SponsorshipForm
from kleemablech.models import User
from kleemablech.models import SponsorshipWin


class SponsorshipView(LoginRequiredMixin, FormView):
    form_class    = SponsorshipForm
    success_url   = '/'
    template_name = 'pages/sponsorship.html'

    def form_valid(self, form):
        guest_email = form.cleaned_data['guest_email']

        if User.objects.filter(email=guest_email).exists():
            return JsonResponse(
                {'form_errors': {'__all__': [_("Email associated with already registred user"), ]}},
                status=400)
        elif Invitation.objects.filter(email=guest_email).exists():
            return JsonResponse(
                {'form_errors': {'__all__': [_("An invitation has already been sent to this email"), ]}},
                status=400)

        form.send_invitation(self.request)

        if self.request.is_ajax():
            return JsonResponse({
                'status'     : _('Invitation succesfuly sent'),
                'form_errors': None
            }, status=200)
        return super(SponsorshipView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        user     = self.request.user
        context  = super(SponsorshipView, self).get_context_data(**kwargs)
        interval = datetime.today() - timedelta(days=config.SPONSORSHIP_PERIODE)

        sponsorships_number = SponsorshipWin.objects.filter(
            Q(user=user) & Q(state='Payed') & Q(date__gte=interval)).count()

        context.update({
            'authorized_sponsorships': config.SPONSORSHIP_LIMIT,
            'remaining_sponsorships' : config.SPONSORSHIP_LIMIT - sponsorships_number,
        })
        return context
