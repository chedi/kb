from operator import itemgetter

from django.views.generic       import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models import Win
from kleemablech.models import Refill
from kleemablech.models import CampaignWin
from kleemablech.models import SponsorshipWin


class HistoryView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/history.html'

    def get_context_data(self, **kwargs):
        user    = self.request.user
        context = super(HistoryView, self).get_context_data(**kwargs)

        operations = sorted(
            [{'type': 'win'        , 'date': win.date        , 'amount': win        .amount} for win         in Win           .objects.filter(user=user)] +
            [{'type': 'draw'       , 'date': draw.date       , 'amount': draw       .amount} for draw        in CampaignWin   .objects.filter(user=user)] +
            [{'type': 'refill'     , 'date': refill.date     , 'amount': refill     .amount} for refill      in Refill        .objects.filter(user=user)] +
            [{'type': 'sponsorship', 'date': sponsorship.date, 'amount': sponsorship.amount} for sponsorship in SponsorshipWin.objects.filter(user=user)] ,
            key=itemgetter('date'), reverse=True)

        context.update({
            'operations': operations
        })

        return context
