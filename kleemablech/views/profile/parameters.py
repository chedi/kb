from django.http                import JsonResponse
from django.utils.translation   import ugettext as _
from django.views.generic.edit  import FormView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.forms  import ParametersForm
from kleemablech.models import User


class ParametersView(LoginRequiredMixin, FormView):
    form_class    = ParametersForm
    template_name = 'pages/parameters.html'

    def get_form_kwargs(self):
        form_kwargs = super(ParametersView, self).get_form_kwargs()
        form_kwargs.update({'user': self.request.user})
        return form_kwargs

    def form_invalid(self, form):
        response = super(ParametersView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(
                {'form_errors': form.errors},
                status=400)
        return response

    def form_valid(self, form):
        cropping = User.objects.get(id=self.request.user.id).cropping
        form.save()
        self.request.user.cropping = cropping
        if form.cleaned_data['password']:
            self.request.user.set_password(form.cleaned_data['password'])
        self.request.user.save()

        if self.request.is_ajax:
            return JsonResponse(
                {'status': _('Les informations ont été mise à jour')},
                status=200)
        return self.render_to_response(self.get_context_data(form=form))
