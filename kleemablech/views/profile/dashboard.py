from datetime import datetime
from datetime import timedelta
from operator import itemgetter

from django.db.models           import Q
from django.db.models           import Sum
from django.views.generic       import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.models        import Win
from kleemablech.models        import Refill
from kleemablech.models        import Campaign
from kleemablech.models        import CampaignView
from kleemablech.utils.strings import month_labels


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/dashboard.html'

    def get_context_data(self, **kwargs):
        user          = self.request.user
        context       = super(DashboardView, self).get_context_data(**kwargs)
        interval      = datetime.today() - timedelta(days=180)
        current_month = datetime.today().month

        data_source = {_: 0 for _ in range (current_month - 6, current_month)}

        data_source.update({
            int(win['month']) - 1: int(win['amount']) for win in
            Win.objects.filter(Q(user=user) & Q(date__gte=interval)).extra(
                select={'month': 'extract(month from date)'}).values(
                    'month').annotate(amount=Sum('amount'))})

        data_source = [{'month': month_labels[win[0] - 1], 'amount': win[1]}
                       for win in sorted(data_source.items(), key=itemgetter(0))]

        context.update({
            'data_source'                          : data_source                                    ,
            'winners_total'                        : Win.winners_total()                            ,
            'user_total_wins'                      : Win.user_total_wins(user)                      ,
            'total_amount_sum'                     : Campaign.total_amount_sum()                    ,
            'user_total_refill'                    : Refill.user_total_refill(user)                 ,
            'total_winable_amount'                 : Campaign.total_winable_amount()                ,
            'campaigns_total_views'                : CampaignView.objects.count()                   ,
            'user_cumulation_remainings'           : Win.user_cumulation_remainings(user)           ,
            'user_total_campaigns_views'           : CampaignView.user_total_campaigns_views(user)  ,
            'user_total_cumulated_credit'          : Win.user_total_cumulated_credit(user)          ,
            'user_cumulation_remainings_percentage': Win.user_cumulation_remainings_percentage(user),
        })

        return context
