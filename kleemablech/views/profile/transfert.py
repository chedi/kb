from math     import modf
from decimal  import Decimal
from datetime import datetime

from django.http                import JsonResponse
from django.utils.crypto        import get_random_string
from django.utils.translation   import ugettext as _
from django.views.generic.edit  import FormView
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.forms          import TransfertForm
from kleemablech.models         import Win
from kleemablech.models         import Refill
from kleemablech.taskapp.celery import refill_task


class TransfertView(LoginRequiredMixin, FormView):
    form_class    = TransfertForm
    success_url   = '/'
    template_name = 'pages/transfert.html'

    def get_user_credit(self, user):
        dec_part, int_part = modf(Win.user_total_cumulated_credit(user))
        return {
            'total_credit'   : int(int_part),
            'credit_dinars'  : int(int_part),
            'credit_millimes': str(int(dec_part * 1000)).zfill(3),
        }

    def get_context_data(self, **kwargs):
        user    = self.request.user
        context = super(TransfertView, self).get_context_data(**kwargs)
        context.update(self.get_user_credit(user))
        return context

    def form_invalid(self, form):
        return JsonResponse(
            {'form_errors': {'__all__': [_('invalid request data'), ]}},
            status=400)

    def form_valid(self, form):
        user   = self.request.user
        number = form.cleaned_data['number']

        import ipdb
        ipdb.set_trace()

        try:
            amount = int(form.cleaned_data['amount'])

            if not user.phonenumber_set.filter(number=number).exists():
                form_errors_msg = _('invalid phone number')
            elif amount < 1:
                form_errors_msg = _('minimal transfert amount is 1 DT')
            elif amount > Win.user_total_cumulated_credit(user):
                form_errors_msg = _('you dont have enough credit for the operation')
            else:
                refill_request = Refill.objects.create(
                    user              = user,
                    date              = datetime.today().now(),
                    amount            = Decimal(form.cleaned_data['amount']),
                    verification_code = get_random_string(length=32))

                refill_task.delay(number, amount, refill_request.verification_code)

                return JsonResponse(
                    {'status'    : _('tranfert operation started successfuly'),
                     'new_credit': self.get_user_credit(user)},
                    status=200)
        except:
            form_errors_msg = _('the amount must be a numerical data')

        return JsonResponse(
            {'form_errors': {'__all__': [form_errors_msg, ]}},
            status=400)
