from django.http                import JsonResponse
from django.http                import HttpResponse
from django.views.generic       import View
from django.utils.translation   import ugettext as _
from django.contrib.auth.mixins import LoginRequiredMixin

from twilio.rest.lookups import TwilioLookupsClient

from kleemablech.forms  import PhoneNumbersForm
from kleemablech.models import PhoneNumber


def check_phone_number_validity(phone_number):
    client = TwilioLookupsClient()
    try:
        if len(phone_number) == 8:
            response = client.phone_numbers.get('+216' + phone_number)
        else:
            response = client.phone_numbers.get(phone_number)
            response.phone_number
        return True
    except:
        return False


class CheckPhoneNumberView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        phone_number  = request.GET.get('phone', None)
        fail_response = HttpResponse(status=400)

        if phone_number:
            phone_number = phone_number.replace(' ', '')
            if PhoneNumber.objects.filter(number=phone_number).exists():
                fail_response.reason_phrase = _("Phone number already used")
            else:
                if check_phone_number_validity(phone_number):
                    return HttpResponse(status=200)
                else:
                    fail_response.reason_phrase = _('Invalid phone number')
        else:
            fail_response.reason_phrase = _('Invalid request')
        return fail_response


class PhoneNumbersView(LoginRequiredMixin, View):
    form_class = PhoneNumbersForm

    def post(self, request, *args, **kwargs):
        phone_form = self.form_class(request.POST.copy())
        phone_form.data['user'] = request.user.id

        if phone_form.is_valid():
            if check_phone_number_validity(phone_form.cleaned_data['number']):
                if PhoneNumber.objects.filter(number=phone_form.cleaned_data['number']).exists():
                    return JsonResponse(
                        {'form_errors': {'__all__': [_("Phone number already used"), ]}},
                        status=400)
                else:
                    phone_form.save()
                    return JsonResponse({'form_errors': None}, status=200)
            else:
                return JsonResponse(
                    {'form_errors': {'__all__': [_('Invalid phone number'), ]}},
                    status=400)
        return JsonResponse({'form_errors': phone_form.errors}, status=400)
