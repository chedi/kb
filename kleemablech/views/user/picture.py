from PIL import Image

from django.http                import JsonResponse
from django.views.generic       import View
from django.template.loader     import render_to_string
from django.utils.translation   import ugettext as _
from django.contrib.auth.mixins import LoginRequiredMixin

from kleemablech.forms import PictureForm


class PictureUploadView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        error     = None
        html_form = None

        if 'picture' not in request.FILES:
            error = _(u"the image has not been correctly transmitted")
        else:
            try:
                tmp_mem = request.FILES['picture']
                picture = Image.open(tmp_mem)

                if picture.format not in ('GIF', 'PNG', 'JPEG'):
                    error = _(u'the image format must be either png, gif or jpeg')
                elif tmp_mem.size > 1024 * 1024:
                    error = _(u'the image size must not exceed 1 Mb')
                else:
                    request.user.picture.save(tmp_mem.name, tmp_mem)
                    if 'cropping' in request.POST:
                        request.user.cropping = request.POST['cropping']
                        request.user.save()

                    html_form = render_to_string('components/image_cropping.html', {
                        'user': request.user,
                        'form': PictureForm(instance=request.user)
                    })
            except:
                error = _(u'the transmitted file is not recognized as a valid image')
        return JsonResponse({'errors': error, 'html_form': html_form})
