from django.views.generic import TemplateView

from kleemablech.models import Topic


class FaqView(TemplateView):
    template_name = 'pages/faq.html'

    def get_context_data(self, **kwargs):
        context = super(FaqView, self).get_context_data(**kwargs)
        context.update({'topics': Topic.objects.all()})
        return context
