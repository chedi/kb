from django.apps import AppConfig


class KleemablechConfig(AppConfig):
    name = 'kleemablech'

    def ready(self):
        from kleemablech.signals import setup_signals
        setup_signals()
