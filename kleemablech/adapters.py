from datetime import datetime

from django.conf import settings

from allauth.account.adapter       import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

from kleemablech.models.choices import REGION_CHOICES
from kleemablech.models.choices import GENDER_CHOICES


class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request, sociallogin):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def populate_user(self, request, sociallogin, data):
        user = super(SocialAccountAdapter, self).populate_user(request, sociallogin, data)

        try:
            birthday      = sociallogin.account.extra_data['birthday']
            user.birthday = datetime.strptime(birthday, "%m/%d/%Y").date()
        except:
            pass

        try:
            location = sociallogin.account.extra_data['location']['name'].split(',')[0]
            if dict(REGION_CHOICES).get(location, None):
                user.region = location
        except:
            pass

        try:
            gender = sociallogin.account.extra_data['gender']
            if dict(GENDER_CHOICES).get(gender, None):
                user.gender = gender
        except:
            pass

        return user
