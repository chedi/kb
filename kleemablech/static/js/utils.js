$(document).ready(function () {
	if(typeof($) === 'undefined' && typeof(jQ) === 'function'){
		$ = jQ;
	}

	window.notify_error = function(message){
		$('.top-left').notify({
			type   : 'danger',
			message: { text: message }
		}).show();
	};

	window.notify_info = function(message){
		$('.top-left').notify({
			message: { text: message }
		}).show();
	};

	window.clean_error_messages = function(form_id){
		$('#' + form_id).find('.with-errors').each(function(index, element){
			$(this).empty();
		});
	};

	window.form_validation_show_errors = function(data, form_id, use_only_notification){
		errors = data.responseJSON.form_errors;
		use_only_notification = typeof use_only_notification !== 'undefined' ?  use_only_notification : false;

		if(data.status == 400){
			if(use_only_notification){
				for(error_field in errors){
					for (error_msg in errors[error_field]){
						notify_error(gettext(error_field) + " : " + gettext(errors[error_field][error_msg]));
				}}
				for(global_error in data.responseJSON.form_errors.__all__){
					notify_error(gettext(data.responseJSON.form_errors.__all__[global_error]));
			}}else{
				clean_error_messages(form_id);
				for(error_field in errors){
					form_group = $('#' + form_id + ' [name='+error_field+']').closest('.form-group');
					error_tag  = form_group.find('.with-errors');
					form_group.addClass('has-error has-danger');

					for (error_msg in errors[error_field]){
						error_tag.append('<p>'+errors[error_field][error_msg]+'</p>');
				}}

				for(global_error in data.responseJSON.form_errors.__all__){
					notify_error(gettext(data.responseJSON.form_errors.__all__[global_error]));
		}}}else{
			notify_error('request failed');
	}};

	window.init_circles = function(){
		stat_mul = $('.Statsmull' );
		stat_glb = $('.Statvuglob');

		if(stat_mul.length > 0){
			stat_mul.circliful();
		}

		if(stat_glb.length > 0){
			stat_glb.circliful();
		}
	};

	window.get_cookie = function(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = $.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}}}
		return cookieValue;
	};

	window.setup_ajax_csrf = function(xhr, settings) {
		if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
			xhr.setRequestHeader("X-CSRFToken", get_cookie('csrftoken'));
		}
	};

	window.get_sharing_informations = function(){
		$.ajax({
			url : $('#share_link')[0].href,
			type: 'GET'
		}).fail(function(data, status){
			window.sharing_enabled = false;
		}).done(function(data, status){
			window.sharing_enabled    = true;
			window.share_informations = data.share_informations;
		});
	};
});
