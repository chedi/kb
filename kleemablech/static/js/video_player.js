$(document).ready(function () {
	jwplayer('player').setup({
		file     : video_media_url,
		width    : "100%",
		controls : false,
		autostart: true
	});

	$(window).on("blur", function (e) {
		jwplayer('player').pause(true);
	});

	$(window).on("focus", function (e) {
		jwplayer('player').pause(false);
	});

	jwplayer('player').on('complete', function (e) {
		window.location = video_counter_url;
	});

	jwplayer('player').on('ready', function (e) {
		adjust_video_height();
	});

	window.adjust_video_height = function(){
		footer_height = $('.footer').height();
		navbar_height = $('.navbar').height();

		player           = $('#player video')[0];
		player_box       = $($('.boxPlay'   )[0]);
		player_container = $($('#player'    )[0]);

		maximal_width     = window.innerWidth;
		maximal_height    = window.innerHeight - navbar_height - footer_height;
		computed_height   = Math.round(video_height * player_container. width() / video_width);
		player_box_margin = (maximal_height - computed_height) / 2;

		player_container.height(computed_height);
	};

	$(window).resize(function() {
		adjust_video_height();
	});
});
