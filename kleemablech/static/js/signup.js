$(document).ready(function($) {
	$('#signup_form').on('submit', function(event) {
		var form = $(this);
		clean_error_messages('signup_form');

		$.ajax({
			url       : form.attr('action'),
			type      : form.attr('method'),
			data      : form.serialize(),
			beforeSend: setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'signup_form');
		}).done(function(data, status){
			window.location = data.location;
		});

		event.preventDefault();
	});
});
