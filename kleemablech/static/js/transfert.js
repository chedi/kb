jQuery(function($) {
	$('#transfert_form').on('submit', function(event){
		var transfert_form = $(this);
		$('#id_number').val($('.flex-active-slide .numchrge').html());
		clean_error_messages('transfert_form');

		$.ajax({
			url       : transfert_form.attr('action'),
			type      : transfert_form.attr('method'),
			data      : transfert_form.serialize(),
			beforeSend: setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'transfert_form');
		}).done(function(data, status){
			notify_info(data.status);
			if(data.new_credit !== undefined){
				$('.creditNum').html(
					data.new_credit.credit_dinars   + "<sup>dt</sup><span>" +
					data.new_credit.credit_millimes + "</span>");
			}
		});
		event.preventDefault();
	});
});
