$(document).ready(function () {
	if(!already_viewed){
		$('#counter')
			.countdown((new Date).getTime() + 10000)
			.on('update.countdown', function (event) {
				var format = '%S';
				$(this).html(event.strftime(format));
			})
			.on('finish.countdown', function(event) {
				window.location = video_timeout_url;
			});
	}
});
