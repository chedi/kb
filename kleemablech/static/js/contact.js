$(document).ready(function () {
	$('#contact_form').on('submit', function(event){
		var contact_form = $(this);
		clean_error_messages('contact_form');

		$.ajax({
			url       : contact_form.attr('action'),
			type      : contact_form.attr('method'),
			data      : contact_form.serialize(),
			beforeSend: setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'contact_form');
		}).done(function(data, status){
			notify_info(data.status);
		});

		grecaptcha.reset();
		event.preventDefault();
	});
});
