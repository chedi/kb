$(document).ready(function () {
	var pusher = new Pusher(pusher_key, {
		cluster: 'eu',
		encrypted: true
	});

	var channel   = pusher.subscribe('winners_channel');
	var template  = $('#winner_template').html();
	Mustache.tags = ['[[', ']]'];

	channel.bind('update_event', function(data) {
		var rendered = Mustache.render(template, data);
		$($('#winners .row')[0]).prepend(rendered);
		$('#winners .row .col-sm-12').last().remove();
	});

	function social_share_video(){
		$('[data-toggle=popover]').popover({
			content  : $('#share_video_popover_content').html(),
			html     : true,
			trigger  : 'click',
			placement: 'up',
			container: 'body'
		});
	}

	$('body').on('click', function (e) {
		$('[data-toggle=popover]').each(function () {
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 &&
			    $('.popover').has(e.target).length === 0
			   ) {
				$(this).popover('hide');
			}
		});
	});

	social_share_video();
});
