$(document).ready(function($) {
	var topic_id       = window.location.hash;
	var first_question = $($(topic_id).children('.title-acc')[0]);

	first_question.next(".content-acc").slideToggle()
		.siblings(".content-acc:visible").slideUp();
	first_question.toggleClass("active");
	first_question.siblings(".title-acc").removeClass("active");
});
