$(document).ready(function() {
	$('#sponsorship_form').on('submit', function(event){
		var sponsorship_form = $(this);
		clean_error_messages('sponsorship_form');

		$.ajax({
			url       : sponsorship_form.attr('action'),
			type      : sponsorship_form.attr('method'),
			data      : sponsorship_form.serialize(),
			beforeSend: setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'sponsorship_form');
		}).done(function(data, status){
			notify_info(data.status);
		});
		event.preventDefault();
	});
});
