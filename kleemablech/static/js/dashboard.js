	$(function () {
		$('#wins_chart_container').highcharts({
			title: {
				text: gettext('User wins'),
				x: -20 //center
			},
			xAxis: {
				categories: chart_categories
			},
			yAxis: {
				title: {
					text: gettext('Wins (DT)')
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				valueSuffix: 'DT'
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				name: gettext('amount'),
				data: chart_data
			},]
		});
	});
