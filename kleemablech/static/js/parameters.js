$(document).ready(function() {
	$('#user_parameters_form').on('submit', function(event){
		var parameters_form = $(this);
		clean_error_messages('user_parameters_form');

		$.ajax({
			url       : parameters_form.attr('action'),
			type      : parameters_form.attr('method'),
			data      : parameters_form.serialize(),
			beforeSend: setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'user_parameters_form');
		}).done(function(data, status){
			notify_info(data.status);
		});
		event.preventDefault();
	});

	$('#new_phone_form').on('submit', function(event) {
		var $form	= $(this);
		var $target = $($form.attr('data-target'));

		$.ajax({
			url       : $form.attr('action'),
			type      : $form.attr('method'),
			data      : $form.serialize(),
			beforeSend: setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'new_phone_form');
		}).done(function(data, status){
			$('#edit_phones').modal('hide');
		});
		event.preventDefault();
	});

	var readURL = function (input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var files  = input.files;

			if(files[0].size > (1024*1024)){
				alert('La taille maximale du ficher ne doit pas être supérieur à 1 Mo');
				return;
			}

			reader.onload = function (e) {
				var data = new FormData();
				data.append('picture', files[0]);

				$.ajax({
					url        : picture_upload_url,
					type       : 'POST',
					data       : data,
					dataType   : 'json',
					beforeSend : setup_ajax_csrf,
					processData: false,
					contentType: false,

					success: function(data, textStatus, jqXHR) {
						if(data.errors === null) {
							$('.profile-pic').attr('src', e.target.result);
							$('#cropping_modal .modal-body').html(data.html_form);
							$('#cropping_modal').modal("show");
							image_cropping.init(jQuery);
						} else {
							$('.top-left').notify({
								type   : 'danger',
								message: { text: data.errors }
							}).show();
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$('.top-left').notify({
							type   : 'danger',
							message: { text: textStatus }
						}).show();
					}
				});
			};
			reader.readAsDataURL(input.files[0]);
		}
	};

	$("#cropping_modal button.btn").on('click', function() {
		var data = new FormData();
		var cropping_form = $($('#cropping_form'));
		var picture_data  = $(".imageUpload #id_picture"    )[0].files[0];
		var cropping_data = $("#cropping_modal #id_cropping")[0].value;

		data.append("picture" , picture_data );
		data.append("cropping", cropping_data);

		$.ajax({
			url        : cropping_form.attr('action'),
			type       : cropping_form.attr('method'),
			data       : data,
			dataType   : 'json',
			beforeSend : setup_ajax_csrf,
			processData: false,
			contentType: false,

			success: function(data, textStatus, jqXHR) {
				if(data.errors === null) {
					$('#cropping_modal .modal-body').html(data.html_form);
					$("#profile-pic").attr("src", $('#cropped_profile_picture').attr('src'));
					$('#cropping_modal').modal("hide");
				} else {
					$('.top-left').notify({
						type   : 'danger',
						message: { text: data.errors }
					}).show();
			}},
			error: function(jqXHR, textStatus, errorThrown) {
				$('.top-left').notify({
					type   : 'danger',
					message: { text: textStatus }
				}).show();
			}
		});
	});

	$(".file-upload input[type=file]").on('change', function() {
		readURL(this);
	});

	$(".addPhotos").on('click', function() {
		$(".file-upload input[type=file]").click();
	});

	$('#update_parameters').on('click', function(event){
		$('#update_parameters'   ).hide();
		$('#read_only_parameters').hide();
		$('#updatable_parameters').show();
		event.preventDefault();
	});
});
