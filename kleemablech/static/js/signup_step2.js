$(document).ready(function() {
	var readURL = function (input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var files  = input.files;

			if(files[0].size > (1024*1024)){
				alert('La taille maximale du ficher ne doit pas être supérieur à 1 Mo');
				return;
			}

			reader.onload = function (e) {
				var data = new FormData();
				data.append('picture', files[0]);

				$.ajax({
					url        : picture_upload_url,
					type       : 'POST',
					data       : data,
					dataType   : 'json',
					beforeSend : setup_ajax_csrf,
					processData: false,
					contentType: false,

					success: function(data, textStatus, jqXHR) {
						if(data.errors === null) {
							$('.profile-pic').attr('src', e.target.result);
							$('#cropping_modal .modal-body').html(data.html_form);
							$('#cropping_modal').modal("show");
							image_cropping.init(jQuery);
						} else {
							notify_error(data.errors);
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						notify_error(textStatus);
					}
				});
			};
			reader.readAsDataURL(input.files[0]);
		}
	};

	$(".file-upload").on('change', function() {
		readURL(this);
	});

	$(".addPhotos").on('click', function() {
		$(".file-upload").click();
	});

	$("#cropping_modal button.btn").on('click', function() {
		var data = new FormData();
		var cropping_form = $($('#cropping_form'));
		var picture_data  = $(".imageUpload #id_picture"    )[0].files[0];
		var cropping_data = $("#cropping_modal #id_cropping")[0].value;

		data.append("picture" , picture_data );
		data.append("cropping", cropping_data);

		$.ajax({
			url        : cropping_form.attr('action'),
			type       : cropping_form.attr('method'),
			data       : data,
			dataType   : 'json',
			beforeSend : setup_ajax_csrf,
			processData: false,
			contentType: false,

			success: function(data, textStatus, jqXHR) {
				if(data.errors === null) {
					$('#cropping_modal .modal-body').html(data.html_form);
					$("#profile-pic").attr("src", $('#cropped_profile_picture').attr('src'));
					$('#cropping_modal').modal("hide");

					if($('#profile_picture_selected')[0].checked == false){
						$('#profile_picture_selected').click();
					}
				} else {
					notify_error(data.errors);
			}},
			error: function(jqXHR, textStatus, errorThrown) {
				notify_error(textStatus);
			}
		});
	});

	$('#signup_step2_form').on('submit', function(event) {
		var form   = $(this);
		var target = $($form.attr('data-target'));

		$.ajax({
			url        : form.attr('action'),
			type       : form.attr('method'),
			data       : form.serialize(),
			beforeSend : setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'signup_step2_form');
		}).done(function(data, status){
			window.location = data.location;
		});
		event.preventDefault();
	});
});
