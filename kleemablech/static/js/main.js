$(document).ready(function () {
	$(document).scroll(function () {
		if ($(document).scrollTop() >= 70) {
			$('.navbar ').addClass('navbar-fixed-top');
			$('html').addClass('has-fixed-nav');
		} else {
			$('.navbar ').removeClass('navbar-fixed-top');
			$('html').removeClass('has-fixed-nav');
		}
	});
	var widowWidth = $(window).width();
	if (widowWidth >= 500) {
		$(document).scroll(function () {
			if ($(document).scrollTop() >= 70) {
				$('.block-submenu ').addClass('submenu-fixed');
				$('.dashboardpage').addClass('padding-fix');
			} else {
				$('.block-submenu ').removeClass('submenu-fixed');
				$('.dashboardpage').removeClass('padding-fix');
			}
		});
	}

	//Close submenu
	$('.subShow').show(300);
	$('.subClose').click(function () {
		$('.block-submenu').slideUp(300);
		$('.subShow').show(300);
	});
	$('.subShow').click(function () {
		$('.block-submenu').slideDown(300);
		$('.subShow').hide(300);
	});

	// Load video thumbs onload
	$('.chart').each(function () {
		var barColor   = "#000000";
		var trackColor = "#ffffff";
		var lineWidth  = "15";
		var lineCap    = "butt";
		var size       = "150";
		var animate    = "";

		$(this).easyPieChart({
			// easing: 'easeOutElastic',
			barColor  : barColor,
			trackColor: trackColor,
			lineWidth : lineWidth,
			lineCap   : lineCap,
			scaleColor: false,
			size      : size,
			animate   : animate,

			onStep: function (from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent));
			},
			delay: 3000
		});
	});
	// Load video thumbs onload
	$('.chart-small').each(function () {
		var barColor = "#ffffff";
		var trackColor = "#999999";
		var lineWidth = "4";
		var lineCap = "butt";
		var size = "48";
		var animate = 1000;

		$(this).easyPieChart({
			// easing: 'easeOutElastic',
			barColor: barColor,
			trackColor: trackColor,
			lineWidth: lineWidth,
			lineCap: lineCap,
			scaleColor: false,
			size: size,
			animate: animate,

			onStep: function (from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent));
			},
			delay: 3000
		});
	});

	$(".img-hover-effect").each(function () {
		var element = $(this);
		element.append("<span class='hoveringel'><i class='fa fa-play-circle'></i></span>").children(".hoveringel").css("line-height", element.children("img").height() + "px").css("font-size", (element.children("img").height() / 3.3) + "px");
	});
	// Load video thumbs onload
	$(".loadingvideo").each(function () {
		var element = $(this),
		    tempimage = new Image(),
		    imagesrc = element.children("img").attr("rel");
		tempimage.src = imagesrc;
		$(tempimage).load(function () {
			element.children("img").css("background-image", "url(" + imagesrc + ")").attr("rel", "").parent().removeClass("loadingvideo");
		});
	});

	// Toogle nav
	$('.tooglenav').click(function () {
		$('.nav-pills').slideToggle(300);
	});

	// Toogle Block infos
	$('.toogle-Infos').addClass('up');
	$('.toogle-Infos').click(function () {
		$('.blocInfos .container').slideToggle(300);
		$('.blocInfos').toggleClass('closed');
		$('.toogle-Infos').toggleClass('up');
	});

	//Slider home flexslider
	if ($('.flexslider').length > 0) {
		$('.flexslider').flexslider({
			animation: "slide",
			directionNav: false,
			manualControls: ".flex-control-nav li a",
			start: function (slider) {
				$('body').removeClass('loading');
			}
		});
	}

	//Slider home flexslider
	if ($('.phone-slide').length > 0) {
		$('.phone-slide').flexslider({
			animation: "slide",
			slideshow: false,
			directionNav: true,
			controlNav: false,
			start: function (slider) {
				$('body').removeClass('loading');
				var numero = slider.slides[slider.currentSlide].children[1].innerHTML;
				$('#id_numero').val(numero);
			},
			after: function (slider) {
				var numero = slider.slides[slider.currentSlide].children[1].innerHTML;
				$('#id_numero').val(numero);
			}
		});
	}

	$('.skillbar').each(function () {
		$(this).find('.skillbar-bar').animate({
			width: jQuery(this).attr('data-percent')
		}, 3000);
	});

	// STAT USER
	var randomScalingFactor = function () {
		return Math.round(Math.random() * 100);
	};

	if(user_logged){
		if ($('#canvas').length > 0) {
			window.onload = function () {
				$.ajax({
					url     : monthly_wins_stats_url,
					dataType: 'json'
				}).done(function(data){
					var lineChartData = {
						labels  : data.labels,
						datasets: [{
							label               : gettext("Monthly win statistics"),
							fillColor           : "rgba(220,220,220,0.0)",
							strokeColor         : "rgba(220,220,220,.8)",
							pointColor          : "rgba(220,220,220,.8)",
							pointStrokeColor    : "#fff",
							pointHighlightFill  : "#fff",
							pointHighlightStroke: "rgba(220,220,220,.8)",
							data: data.values
						}]
					};

					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartData, {responsive: true});
				});
			};
		}

		if ($('#canvas-week').length > 0) {
			$.ajax({
				url     : monthly_refills_stats_url,
				dataType: 'json'
			}).done(function(data) {
				var line_refill_total = {
					labels: data.labels,
					datasets: [{
						label               : gettext("Monthly refills stats"),
						fillColor           : "rgba(220,220,220,0.0)",
						strokeColor         : "rgba(255,76,121,1)",
						pointColor          : "rgba(255,76,121,1)",
						pointStrokeColor    : "#fff",
						pointHighlightFill  : "#fff",
						pointHighlightStroke: "rgba(220,220,220,.8)",
						data: data.values
					}]
				};

				var ctx2 = document.getElementById("canvas-week").getContext("2d");
				window.myLine = new Chart(ctx2).Line(line_refill_total, {responsive: true});
			});
		}
	}
	//End STAT USER

	// Scroll winners*
	if($('#winners').length >0 ){
		$("#winners").mCustomScrollbar({
			scrollButtons:{enable:true},
			theme:"light-thick",
			scrollbarPosition:"outside"
		});
	}
	if($('#hystoryBox').length >0 ){
		$("#hystoryBox").mCustomScrollbar({
			scrollButtons:{enable:true},
			theme:"light-thick",
			scrollbarPosition:"outside"
		});
	}

	// Accordion
	$(".accordion .content-acc ").hide();

	$(".accordion .title-acc").click(function () {
		$(this).next(".content-acc").slideToggle()
			.siblings(".content-acc:visible").slideUp();
		$(this).toggleClass("active");
		$(this).siblings(".title-acc").removeClass("active");
	});

	// Custom Radio Check: jQuery plguin for checkbox and radio replacement
	if ($('.connexion input[type=checkbox], .checked-option input[type=radio], .felicit-form input[type=checkbox]').length > 0) {
		$('.connexion input[type=checkbox], .checked-option input[type=radio], .felicit-form input[type=checkbox]').customRadioCheck();
	}

	// select taille
	if ($('select').length > 0) {
		$(function () {
			combo_selects = $('select').not('.no-combo-select');
			if(combo_selects.length > 0){
				combo_selects.comboSelect();
			}
		});
	}

	$(".choisexe .male").click(function () {
		$(this).addClass("opened");
		$(".choisexe .femelle").removeClass("opened");
	});
	$(".choisexe .femelle").click(function () {
		$(this).addClass("opened");
		$(".choisexe .male").removeClass("opened");
	});


	if ($('#bgndVideo').length > 0) {
		$('#bgndVideo').on("YTPReady", function (e) {
			alert('12seconde');
			var currentTime = e.time;
			if (currentTime == 12) {
				console.log("12secone");
			}
		});
	}
	if ($('#clock').length > 0) {
		$('#clock').countdown('2016/03/10', function (event) {
			$(this).html(event.strftime(''
			                            + '<span>%-w <span class="dateStyle">week%!w</span></span>  '
			                            + '<span>%-d <span class="dateStyle">day%!d</span></span> '
			                            + '<span>%H <span class="dateStyle">hr</span></span> '
			                            + '<span>%M <span class="dateStyle">min</span></span>  '
			                            + '<span>%S <span class="dateStyle">sec</span></span> '));
		});
	}

	$('.refill_disabled').click(function(e) {
		e.preventDefault();
	});

	$('#news_letter_form').on('submit', function(event){
		var news_letter_form = $(this);
		clean_error_messages('news_letter_form');

		$.ajax({
			url       : news_letter_form.attr('action'),
			type      : news_letter_form.attr('method'),
			data      : news_letter_form.serialize(),
			beforeSend: setup_ajax_csrf
		}).fail(function(data, status){
			form_validation_show_errors(data, 'news_letter_form', true);
		}).done(function(data, status){
			notify_info(data.status);
		});
		event.preventDefault();
	});

	window.get_twitter_followers = function(){
		$.ajax({
			url : twitter_followers_check_url,
			type: 'GET'
		}).fail(function(data, status){
			$('#twitter_followers_count'       ).html('0');
			$('#footer_twitter_followers_count').html('0');
			console.log(data);
		}).done(function(data, status){
			$('#twitter_followers_count'       ).html(data.follower_count);
			$('#footer_twitter_followers_count').html(data.follower_count);
		});
	};

	window.get_facebook_followers = function(){
		$.ajax({
			url : facebook_followers_check_url,
			type: 'GET'
		}).fail(function(data, status){
			$('#facebook_followers_count'      ).html('0');
			$('#footer_facebook_followers_count').html('0');
			console.log(data);
		}).done(function(data, status){
			$('#facebook_followers_count'      ).html(data.follower_count);
			$('#footer_facebook_followers_count').html(data.follower_count);
		});
	};

	$('#share_link').on('click', function(event){
		event.preventDefault();
		if(sharing_enabled){
			$.ajax({
				url       : $('#share_link')[0].href,
				type      : 'POST',
				data      : {
					share_id: share_informations.id,
					csrfmiddlewaretoken: csrf_token
				},
				beforeSend: setup_ajax_csrf
			}).fail(function(data, status){
				msg    = "";
				errors = data.responseJSON.form_errors;
				for(error_field in errors){
					for (error_msg in errors[error_field]){
						msg += gettext(errors[error_field][error_msg]);
					}
				}
				alert(msg);
			}).done(function(data, status){
				console.log(data);
				FB.ui({
					method     : 'feed'                    ,
					name       : share_informations.name   ,
					link       : share_informations.link   ,
					picture    : share_informations.picture,
					caption    : share_informations.caption,
					description: share_informations.message
				}, function(response) {
					if (response && response.post_id) {
						alert(gettext('Post was published'));
					} else {
						alert(gettext('Post was not published'));
					}
				});
				get_sharing_informations();
			});
		}else if(! is_authenticated){
			location.pathname=login_url;
		}else{
			alert(gettext("You have consumed your daily sharing quota"));
		}
	});

	init_circles();

	get_twitter_followers   ();
	get_facebook_followers  ();
	get_sharing_informations();

	window.twitter_followers_check_interval  = setInterval(get_twitter_followers   , 600000);
	window.facebook_followers_check_interval = setInterval(get_facebook_followers  , 600000);
	window.facebook_followers_check_interval = setInterval(get_sharing_informations, 600000);
});
