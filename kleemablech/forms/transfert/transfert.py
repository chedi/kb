from django.forms import Form
from django.forms import CharField
from django.forms import HiddenInput
from django.forms import NumberInput


class TransfertForm(Form):
    number = CharField(max_length=50, widget=HiddenInput)
    amount = CharField(max_length=50, widget=NumberInput)
