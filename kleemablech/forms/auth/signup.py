from django.forms import Form
from django.forms import ModelForm
from django.forms import CharField
from django.forms import DateField
from django.forms import EmailField
from django.forms import ChoiceField

from kleemablech.models.user    import User
from kleemablech.models.choices import GENDER_CHOICES
from kleemablech.models.choices import REGION_CHOICES


class SignupForm(Form):
    email      = EmailField()
    gender     = ChoiceField(choices=GENDER_CHOICES )
    region     = ChoiceField(choices=REGION_CHOICES)
    birthday   = DateField()
    password1  = CharField(max_length=255)
    password2  = CharField(max_length=255)
    last_name  = CharField(max_length=255)
    first_name = CharField(max_length=255)

    class Meta:
        model = User

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)

        if hasattr(self, 'sociallogin'):
            self.initial['gender'  ] = self.sociallogin.user.gender
            self.initial['region'  ] = self.sociallogin.user.region
            self.initial['birthday'] = self.sociallogin.user.birthday

    def signup(self, request, user):
        user.email      = self.cleaned_data['email'     ]
        user.gender     = self.cleaned_data['gender'    ]
        user.region     = self.cleaned_data['region'    ]
        user.birthday   = self.cleaned_data['birthday'  ]
        user.last_name  = self.cleaned_data['last_name' ]
        user.first_name = self.cleaned_data['first_name']

        user.set_password(self.cleaned_data['password1'])
        user.save()


class Step2Form(ModelForm):
    phone = CharField(max_length=20)

    class Meta:
        model  = User
        fields = ('picture', 'cropping')
