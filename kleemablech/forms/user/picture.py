from django.forms import ModelForm

from kleemablech.models.user import User


class PictureForm(ModelForm):
    class Meta:
        model  = User
        fields = ('picture', 'cropping')
