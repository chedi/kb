from django.forms             import ModelForm
from django.core.exceptions   import NON_FIELD_ERRORS
from django.utils.translation import gettext_lazy as _

from kleemablech.models import PhoneNumber


class PhoneNumbersForm(ModelForm):
    class Meta:
        model          = PhoneNumber
        fields         = ['user', 'number']
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': _(u"Phone number already used"),
            }
        }
