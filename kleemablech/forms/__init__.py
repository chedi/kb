from kleemablech.forms.auth.signup             import Step2Form
from kleemablech.forms.auth.signup             import SignupForm
from kleemablech.forms.user.picture            import PictureForm
from kleemablech.forms.contact.contact         import ContactForm
from kleemablech.forms.profile.history         import HistoryForm
from kleemablech.forms.home.news_letter        import NewsletterEmailsForm
from kleemablech.forms.user.phone_number       import PhoneNumbersForm
from kleemablech.forms.profile.parameters      import ParametersForm
from kleemablech.forms.transfert.transfert     import TransfertForm
from kleemablech.forms.sponsorship.sponsorship import SponsorshipForm


__all__ = [
    'Step2Form'           ,
    'SignupForm'          ,
    'PictureForm'         ,
    'ContactForm'         ,
    'HistoryForm'         ,
    'TransfertForm'       ,
    'ParametersForm'      ,
    'SponsorshipForm'     ,
    'PhoneNumbersForm'    ,
    'NewsletterEmailsForm',
]
