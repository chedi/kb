from django.conf      import settings
from django.forms     import Form
from django.forms     import Textarea
from django.forms     import CharField
from django.forms     import EmailField
from django.forms     import ChoiceField
from django.core.mail import send_mail

from snowpenguin.django.recaptcha2.fields  import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget


class ContactForm(Form):
    SUBJECT_CHOICES = (
        ('Option1', 'Option1'),
        ('Option2', 'Option2'),
        ('Option3', 'Option3'))

    name    = CharField()
    email   = EmailField()
    subjet  = ChoiceField(choices=SUBJECT_CHOICES)
    message = CharField(widget=Textarea)
    captcha = ReCaptchaField(widget=ReCaptchaWidget())

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ContactForm, self).__init__(*args, **kwargs)
        if user.id:
            self.initial['name' ] = user.get_full_name()
            self.initial['email'] = user.email

    def send_email(self):
        subject    = self.cleaned_data['subject']
        message    = self.cleaned_data['message']
        from_email = self.cleaned_data['email'  ]

        send_mail(subject        = subject,
                  message        = message,
                  from_email     = from_email,
                  recipient_list = settings.CONTACT_EMAIL)
