from django.forms             import ModelForm
from django.utils.translation import gettext_lazy as _

from kleemablech.models import NewsletterEmails


class NewsletterEmailsForm(ModelForm):
    class Meta:
        model  = NewsletterEmails
        fields = ['email', ]
        error_messages = {
            'email': {
                u'unique': _('This address has already been added.'),
            }
        }
