from django.forms import Form
from django.forms import CharField
from django.forms import EmailField

from invitations.models import Invitation

from django.utils.translation import gettext_lazy as _


class SponsorshipForm(Form):
    guest_name  = CharField (label=_(u'your friend name' ), max_length=255)
    guest_email = EmailField(label=_(u'your friend email'))

    def send_invitation(self, request):
        invitation = Invitation.create(self.cleaned_data['guest_email'], inviter=request.user)
        invitation.send_invitation(request)
