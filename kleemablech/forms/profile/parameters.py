from django.forms import ModelForm
from django.forms import PasswordInput

from kleemablech.models import User


class ParametersForm(ModelForm):
    class Meta:
        model   = User
        fields  = ['gender', 'first_name', 'last_name', 'password',
                   'birthday', 'email', 'last_name', 'region',
                   'picture', 'cropping']
        widgets = {
            'password': PasswordInput,
        }

    def __init__(self, *args, **kwargs):
        kwargs['instance'] = kwargs.pop('user')
        super(ParametersForm, self).__init__(*args, **kwargs)
        self.fields['password'].required = False
