from decimal                import Decimal
from datetime               import datetime
from easy_thumbnails.files  import get_thumbnailer
from dateutil.relativedelta import relativedelta

from django.db.models           import Q
from django.db.models           import Sum
from django.db.models           import Count
from django.db.models           import Model
from django.db.models           import CharField
from django.db.models           import ImageField
from django.db.models           import ForeignKey
from django.db.models           import FileField
from django.db.models           import PositiveIntegerField
from django.db.models           import BooleanField
from django.db.models           import DateField
from django.db.models           import DecimalField
from django.utils.html          import mark_safe
from django.core.exceptions     import ValidationError
from django.utils.translation   import gettext_lazy as _
from django.db.models.functions import Coalesce

from kleemablech.models.win     import CampaignWin
from kleemablech.models.user    import User
from kleemablech.models.refill  import Refill
from kleemablech.utils.strings  import month_labels
from kleemablech.models.choices import VIDEO_POSITION_CHOICES


class Campaign(Model):
    draw                      = BooleanField        (_('draw'                     ), default=False                                )
    title                     = CharField           (_('title'                    ), max_length=255                               )
    image                     = ImageField          (_('image'                    ), upload_to='campaigns/image'                  )
    video                     = FileField           (_('video'                    ), upload_to='campaigns/video'                  )
    brand                     = CharField           (_('brand'                    ), max_length=255                               )
    active                    = BooleanField        (_('active'                   ), default=True                                 )
    duration                  = PositiveIntegerField(_('duration'                 )                                               )
    position                  = CharField           (_('position'                 ), max_length=50, choices=VIDEO_POSITION_CHOICES)
    start_date                = DateField           (_('start date'               )                                               )
    draw_amount               = DecimalField        (_('draw amount'              ), decimal_places=3, max_digits=9               )
    total_amount              = PositiveIntegerField(_('total amount'             )                                               )
    total_winners             = PositiveIntegerField(_('total winners'            )                                               )
    unique_visitors           = PositiveIntegerField(_('unique visitors'          )                                               )
    direct_win_amount         = DecimalField        (_('direct win amount'        ), decimal_places=3, max_digits=9               )
    cumulative_win_amount     = DecimalField        (_('cumulative win amount'    ), decimal_places=3, max_digits=9               )
    direct_win_percentage     = PositiveIntegerField(_('direct win percentage'    )                                               )
    campaign_end_draw_amount  = DecimalField        (_('campaign end draw amount' ), decimal_places=3, max_digits=9               )
    cumulative_win_percentage = PositiveIntegerField(_('cumulative win percentage')                                               )

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Campaign' )
        verbose_name_plural = _('Campaigns')

    def __str__(self):
        return self.title

    def clean(self):
        if self.direct_win_percentage + self.cumulative_win_percentage != 100:
            raise ValidationError(_('The sum of direct and cumultative win percentage must equal 100'))

    @staticmethod
    def total_amount_sum():
        return int(Campaign.objects.filter(active=True).aggregate(
            amount=Coalesce(Sum('total_amount'), 0))['amount'])

    @staticmethod
    def total_winable_amount():
        return Campaign.total_amount_sum() - int(
            CampaignWin.objects.filter(verified=True).aggregate(
                amount=Coalesce(Sum('amount'), 0))['amount'])

    @staticmethod
    def total_winable_percentage():
        total_amount_sum     = Decimal(Campaign.total_amount_sum    ())
        total_winable_amount = Decimal(Campaign.total_winable_amount())

        if total_amount_sum != 0 and total_winable_amount > total_amount_sum:
            return int((total_winable_amount / total_amount_sum) * 100)
        return 0

    def winable_percentage(self):
        amount = self.total_amount - CampaignWin.total_campaign_wins(self)
        if self.total_amount != 0 and amount > 0:
            return int(amount / self.total_amount * 100)
        return 0

    def total_refills(self):
        return Refill.campaign_total_refill(self)

    def total_views(self):
        return CampaignView.total_campaign_views(self)

    def winners_total(self):
        return CampaignWin.total_campaign_winners(self)

    def image_img(self):
        if self.image:
            return mark_safe('<img src="{}" />'.format(
                get_thumbnailer(self.image).get_thumbnail(
                    {'size': (200, 200), 'crop': True}).url))
        else:
            return _('no image')
    image_img.short_description = 'Thumb'

    def video_video(self):
        if self.video:
            return mark_safe(
                '<video height="250" controls><source src="{}"></source></video>'.format(
                    self.video.url))
        else:
            return _('no video')
    video_video.short_description = 'Video media'


class CampaignView(Model):
    date = DateField(_('date'), auto_now_add=True)

    user     = ForeignKey(User    , verbose_name=_('user'    ))
    campaign = ForeignKey(Campaign, verbose_name=_('campaign'))

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Campaign View' )
        verbose_name_plural = _('Campaign Views')

    def __str__(self):
        return u'{} - {} - {}'.format(self.date, self.user, self.campaign)

    @staticmethod
    def monthly_user_stats(user):
        seven_months = datetime.today() + relativedelta(day=1, months=-6)
        views = CampaignView.objects.filter(
            Q(user=user                   ) &
            Q(date__gt=seven_months.date())
        ).extra(select={
            'year' : 'EXTRACT(year  FROM date)',
            'month': 'EXTRACT(month FROM date)',
        }).values('year', 'month').annotate(num_views=Count('date')).order_by('year', 'month')

        temp   = {view['month']: view for view in views}
        labels = []
        values = []

        for x in range(0, 7):
            month = (seven_months + relativedelta(months=+x)).month
            labels.append(month_labels[month - 1])
            if month in temp:
                values.append(temp[month]['num_views'])
            else:
                values.append(0)
        return {'labels': labels, 'values': values}

    @staticmethod
    def user_total_campaigns_views(user):
        return CampaignView.objects.filter(user=user).values('campaign').distinct().count()

    @staticmethod
    def total_campaign_views(campaign):
        return CampaignView.objects.filter(campaign=campaign).aggregate(
            total_views=Coalesce(Count('id'), 0))['total_views']
