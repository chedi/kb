from django.db.models         import Model
from django.db.models         import EmailField
from django.db.models         import BooleanField
from django.utils.translation import ugettext_lazy as _


class NewsletterEmails(Model):
    email  = EmailField  (_('email'), max_length=100, unique=True)
    active = BooleanField(_('active'), default=True)

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('News letter email' )
        verbose_name_plural = _('News letter emails')

    def __str__(self):
        return self.email
