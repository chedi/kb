from datetime import datetime

from django.db                  import models
from django.db.models           import Q
from django.db.models           import Sum
from django.db.models           import Model
from django.db.models           import CharField
from django.db.models           import ForeignKey
from django.db.models           import BooleanField
from django.db.models           import DateTimeField
from django.db.models           import DecimalField
from dateutil.relativedelta     import relativedelta
from django.utils.translation   import gettext_lazy as _
from django.db.models.functions import Coalesce

from kleemablech.models.refill   import Refill
from kleemablech.utils.strings   import month_labels
from kleemablech.models.choices  import SPONSORSHIP_WIN_STATE


class Win(Model):
    date   = DateTimeField(_('date'  ), auto_now_add=True             )
    amount = DecimalField (_('amount'), decimal_places=3, max_digits=9)

    user = models.ForeignKey('User', verbose_name=_("User"))

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Win' )
        verbose_name_plural = _('Wins')

    def __str__(self):
        return u"{} - {} - {}".format(self.user, self.date, self.amount)

    @staticmethod
    def user_total_wins(user):
        return Win.objects.filter(user=user).aggregate(total=Coalesce(Sum('amount'), 0))['total']

    @staticmethod
    def user_cumulation_remainings(user):
        cumulation_remainings = int((1 - Win.user_total_wins(user)) * 1000)
        cumulation_remainings = cumulation_remainings if cumulation_remainings > 0 else 0
        return cumulation_remainings

    @staticmethod
    def user_cumulation_remainings_percentage(user):
        return (1000 - Win.user_cumulation_remainings(user)) / 10

    @staticmethod
    def user_total_cumulated_credit(user):
        return Win.user_total_wins(user) - Refill.user_total_refill(user)

    @staticmethod
    def winners_total():
        return Win.objects.values('user').distinct().count()

    @staticmethod
    def monthly_user_stats(user):
        seven_months = datetime.today() + relativedelta(day=1, months=-6)
        wins = Win.objects.filter(
            Q(user=user                   ) &
            Q(date__gt=seven_months.date())
        ).extra(select={
            'year' : 'EXTRACT(year  FROM date)',
            'month': 'EXTRACT(month FROM date)',
        }).values('year', 'month').annotate(amount=Sum('amount')).order_by('year', 'month')

        temp   = {win['month']: win for win in wins}
        labels = []
        values = []

        for x in range(0, 7):
            month = (seven_months + relativedelta(months=+x)).month
            labels.append(month_labels[month - 1])
            if month in temp:
                values.append(int(temp[month]['amount'] * 1000))
            else:
                values.append(0)
        return {'labels': labels, 'values': values}


class CampaignWin(Model):
    date              = DateTimeField(_('date'             ), auto_now_add=True             )
    amount            = DecimalField (_('amount'           ), decimal_places=3, max_digits=9)
    verified          = BooleanField (_('verified'         ), default=False                 )
    verification_code = CharField    (_('verification code'), max_length=64                 )

    user     = ForeignKey('User'    , verbose_name=_('user'    ))
    campaign = ForeignKey('Campaign', verbose_name=_('campaign'))

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Campaign Win' )
        verbose_name_plural = _('Campaign Wins')

    def __str__(self):
        return u"{} - {}".format(self.amount, self.date)

    @staticmethod
    def total_campaign_winners(campaign):
        return CampaignWin.objects.filter(
            Q(verified = True    ) &
            Q(campaign = campaign)
        ).values('user').distinct().count()

    @staticmethod
    def total_campaign_wins(campaign):
        return CampaignWin.objects.filter(
            Q(verified = True    ) &
            Q(campaign = campaign)
        ).aggregate(amount=Coalesce(Sum('amount'), 0))['amount']


class SponsorshipWin(models.Model):
    date   = models.DateTimeField(_('date'  ), auto_now_add=True                           )
    state  = models.CharField    (_('state' ), max_length=50, choices=SPONSORSHIP_WIN_STATE)
    amount = models.DecimalField (_('amount'), decimal_places=3, max_digits=9              )

    user = models.ForeignKey('User')

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Sponsorship Win' )
        verbose_name_plural = _('Sponsorship Wins')
