from django.db.models         import Model
from django.db.models         import CharField
from django.db.models         import TextField
from django.db.models         import ForeignKey
from django.db.models         import PositiveIntegerField
from django.utils.translation import ugettext_lazy as _


class Topic(Model):
    text  = CharField           (_('text'), max_length=200)
    order = PositiveIntegerField(_('order'))

    class Meta:
        ordering            = ['order']
        app_label           = 'kleemablech'
        verbose_name        = _('Topic' )
        verbose_name_plural = _('Topics')

    def __str__(self):
        return u'({}) {}'.format(self.order, self.text)


class Question(Model):
    text   = TextField           (_('text'  ))
    order  = PositiveIntegerField(_('order' ))
    answer = TextField           (_('answer'))

    topic = ForeignKey(Topic, related_name='questions', verbose_name=_('topic'))

    class Meta:
        ordering            = ['order']
        app_label           = 'kleemablech'
        verbose_name        = _('Question' )
        verbose_name_plural = _('Questions')

    def __str__(self):
        return u'({}) {}'.format(self.order, self.text)


class FeaturedTopic(Model):
    text  = TextField           (_('text' ))
    order = PositiveIntegerField(_('order'))

    topic = ForeignKey(Topic, related_name='featured', verbose_name=_('topic'))

    class Meta:
        ordering            = ['order']
        app_label           = 'kleemablech'
        verbose_name        = _('Featured topic' )
        verbose_name_plural = _('Featured topics')

    def __str__(self):
        return u'({}) {}'.format(self.order, self.topic.text)
