from django.db.models         import Q
from django.db.models         import Model
from django.db.models         import CharField
from django.db.models         import TextField
from django.db.models         import ImageField
from django.db.models         import ForeignKey
from django.db.models         import BooleanField
from django.db.models         import DateTimeField
from django.utils.html        import mark_safe
from django.core.exceptions   import ValidationError
from django.utils.translation import ugettext_lazy as _

from constance             import config
from easy_thumbnails.files import get_thumbnailer

from kleemablech.models.user import User


class FacebookShare(Model):
    date    = DateTimeField(_('date'   ), auto_now_add=True )
    name    = CharField    (_('name'   ), max_length=200    )
    link    = TextField    (_('link'   )                    )
    active  = BooleanField (_('active' ), default=True      )
    message = TextField    (_('text'   )                    )
    caption = TextField    (_('caption')                    )
    picture = ImageField   (_('picture'), upload_to='shares')

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Facebook Share' )
        verbose_name_plural = _('Facebook Shares')

    def __str__(self):
        return u"{} - {}".format(self.date, self.name)

    def picture_img(self):
        if self.picture:
            return mark_safe('<img src="{}" />'.format(
                get_thumbnailer(self.picture).get_thumbnail(
                    {'size': (200, 200), 'crop': True}).url))
        else:
            return _('no image')
    picture_img.short_description = 'Thumb'


class UserFacebookShare(Model):
    date = DateTimeField(_('date'))

    user  = ForeignKey(User         , verbose_name=_('user' ))
    share = ForeignKey(FacebookShare, verbose_name=_('share'))

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('User Facebook Share' )
        verbose_name_plural = _('User Facebook Shares')

    def __str__(self):
        return u"{} - {} - {}".format(self.date, self.user, self.share)

    def clean(self, *args, **kwargs):
        queryset = UserFacebookShare.objects.filter(
            Q(user      =self.user       ) &
            Q(share     =self.share      ) &
            Q(date__date=self.date.date()))

        if len(queryset) >= config.MAX_DAYLY_SHARES:
            raise ValidationError(_('User already consumed the daily quota for this share'))

        super(UserFacebookShare, self).clean(*args, **kwargs)
