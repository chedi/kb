from django.db.models import Model
from django.db.models import CharField
from django.db.models import BooleanField
from django.db.models import PositiveIntegerField

from django.utils.translation import gettext_lazy as _


class Operator(Model):
    name             = CharField           (_('name'            ), max_length=50)
    device           = CharField           (_('device'          ), max_length=50)
    prefixes         = CharField           (_('prefixes'        ), max_length=50)
    ussd_string      = CharField           (_('ussd string'     ), max_length=50)
    requires_reply   = BooleanField        (_('requires reply'  ), default=False)
    device_baud_rate = PositiveIntegerField(_('device baud rate')               )

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Operator' )
        verbose_name_plural = _('Operators')

    def __str__(self):
        return self.name
