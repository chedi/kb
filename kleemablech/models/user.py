from django.utils               import timezone
from django.core.mail           import send_mail
from django.db.models           import CharField
from django.db.models           import DateField
from django.db.models           import EmailField
from django.db.models           import BooleanField
from django.db.models           import DateTimeField
from django.utils.translation   import ugettext_lazy as _
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin

from image_cropping.fields import ImageCropField
from image_cropping.fields import ImageRatioField

from kleemablech.models.choices import GENDER_CHOICES
from kleemablech.models.choices import REGION_CHOICES


class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        if not email:
            raise ValueError(_('The email must be set'))
        email = self.normalize_email(email)

        now  = timezone.now()
        user = self.model(email=email, is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email       = EmailField     (_('email'       ), max_length=254, unique=True)
    gender      = CharField      (_('gender'      ), max_length=50, choices=GENDER_CHOICES)
    region      = CharField      (_('region'      ), max_length=255, choices=REGION_CHOICES)
    picture     = ImageCropField (_('picture'     ), upload_to='profile', default='profile/default.png')
    username    = CharField      (_('Username'    ), max_length=30, unique=True)
    is_staff    = BooleanField   (_('staff status'), default=False)
    birthday    = DateField      (_('birth day'   ), null=True, blank=True)
    is_active   = BooleanField   (_('active'      ), default=True)
    last_name   = CharField      (_('last name'   ), max_length=30, blank=True)
    first_name  = CharField      (_('first name'  ), max_length=30, blank=True)
    date_joined = DateTimeField  (_('date joined' ), blank=True, null=True)

    cropping = ImageRatioField('picture', '255x255')

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('user' )
        verbose_name_plural = _('users')

    def save(self, *args, **kwargs):
        if not self.id:
            self.date_joined = timezone.now()
        return super(User, self).save(*args, **kwargs)

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    def __str__(self):
        return u"{} {} - {}".format(self.first_name, self.last_name, self.email)

    def get_full_name(self):
        return u"{} {}".format(self.first_name, self.last_name)
