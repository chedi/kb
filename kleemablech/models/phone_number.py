from django.db.models         import Model
from django.db.models         import CharField
from django.db.models         import ForeignKey
from django.utils.translation import ugettext_lazy as _

from kleemablech.models.user import User


class PhoneNumber(Model):
    number = CharField (_('number'), max_length=50)

    user = ForeignKey(User, verbose_name=_('user'))

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Phone number' )
        unique_together     = (("user", "number"),)
        verbose_name_plural = _('Phone numbers')

    @property
    def operator(self):
        return {
            '5': u'orange',
            '2': u'ooredoo',
            '9': u'tunisie_telecom',
            '4': u'tunisie_telecom',
        }.get(self.number[0], u'unknown')

    def __str__(self):
        return self.number
