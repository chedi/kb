from django.utils.translation import ugettext_lazy as _

VIDEO_POSITION_CHOICES = (
    ('slider'    , _('Slider'    )),
    ('drawing'   , _('Drawing'   )),
    ('subsidized', _('Subsidized')))

SPONSORSHIP_WIN_STATE = (
    ('Payed'  , _('Payed'  )),
    ('Waiting', _('Waiting')))

REGION_CHOICES = (
    ('Ariana'     , _('Ariana'     )),
    ('Beja'       , _('Beja'       )),
    ('Ben Arous'  , _('Ben Arous'  )),
    ('Bizerte'    , _('Bizerte'    )),
    ('Jendouba'   , _('Jendouba'   )),
    ('Gabes'      , _('Gabes'      )),
    ('Gafsa'      , _('Gafsa'      )),
    ('Kairouan'   , _('Kairouan'   )),
    ('Kasserine'  , _('Kasserine'  )),
    ('Kebili'     , _('Kebili'     )),
    ('Le Kef'     , _('Le Kef'     )),
    ('La Manouba' , _('La Manouba' )),
    ('Mahdia'     , _('Mahdia'     )),
    ('Medenine'   , _('Medenine'   )),
    ('Monastir'   , _('Monastir'   )),
    ('Nabeul'     , _('Nabeul'     )),
    ('Sfax'       , _('Sfax'       )),
    ('Sidi Bouzid', _('Sidi Bouzid')),
    ('Siliana'    , _('Siliana'    )),
    ('Sousse'     , _('Sousse'     )),
    ('Tunis'      , _('Tunis'      )),
    ('Tataouine'  , _('Tataouine'  )),
    ('Tozeur'     , _('Tozeur'     )),
    ('Zaghouan'   , _('Zaghouan'   )))

GENDER_CHOICES = (
    ('male'  , _('male'  )),
    ('female', _('female')))
