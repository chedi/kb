from datetime               import datetime
from dateutil.relativedelta import relativedelta

from django.utils               import timezone
from django.db.models           import Q
from django.db.models           import Sum
from django.db.models           import Model
from django.db.models           import CharField
from django.db.models           import ForeignKey
from django.db.models           import BooleanField
from django.db.models           import DateTimeField
from django.db.models           import DecimalField
from django.utils.translation   import gettext_lazy as _
from django.db.models.functions import Coalesce

from kleemablech.models.user   import User
from kleemablech.utils.strings import month_labels


class Refill(Model):
    date              = DateTimeField(_('date'             )                                )
    amount            = DecimalField (_('amount'           ), decimal_places=3, max_digits=9)
    verfied           = BooleanField (_('verfied'          ), default=False                 )
    verification_code = CharField    (_('verification code'), max_length=64                 )

    user = ForeignKey(User, verbose_name=_('user'))

    class Meta:
        app_label           = 'kleemablech'
        verbose_name        = _('Refill' )
        verbose_name_plural = _('Refills')

    def __str__(self):
        return u"{}-{}".format(self.amount, self.date)

    def save(self, *args, **kwargs):
        if not self.id:
            self.date = timezone.now()
        return super(Refill, self).save(*args, **kwargs)

    @staticmethod
    def user_total_refill(user):
        return Refill.objects.filter(user=user).aggregate(
            total=Coalesce(Sum('amount'), 0))['total']

    @staticmethod
    def campaign_total_refill(campaign):
        return Refill.objects.filter(campaign=campaign).aggregate(
            total=Coalesce(Sum('amount'), 0))['total']

    @staticmethod
    def monthly_user_stats(user):
        seven_months = datetime.today() + relativedelta(day=1, months=-6)
        refills = Refill.objects.filter(
            Q(user     = user               ) &
            Q(date__gt = seven_months.date())
        ).extra(select={
            'year' : 'EXTRACT(year  FROM date)',
            'month': 'EXTRACT(month FROM date)',
        }).values('year', 'month').annotate(amount=Sum('amount')).order_by('year', 'month')

        temp   = {refill['month']: refill for refill in refills}
        labels = []
        values = []

        for x in range(0, 7):
            month = (seven_months + relativedelta(months=+x)).month
            labels.append(month_labels[month - 1])
            if month in temp:
                values.append(int(temp[month]['amount'] * 1000))
            else:
                values.append(0)
        return {'labels': labels, 'values': values}
