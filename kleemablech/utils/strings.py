from decimal import Decimal

month_labels = [
    u'Jan', u'Fev', u'Mar', u'Avr', u'Mai', u'Jun',
    u'Jul', u'Aou', u'Sep', u'Oct', u'Nov', u'Dec']


def decimal_to_string(value, digits=3, separator=','):
    quantization = Decimal(10) ** -digits
    return str(Decimal(value).quantize(quantization)).replace('.', separator)
