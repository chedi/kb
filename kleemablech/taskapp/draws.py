from random      import randrange
from datetime    import datetime
from datetime    import timedelta
from collections import defaultdict

from django.db.models import Q


def hourly_draw(*args, **kwargs):
    from kleemablech.models import Win
    from kleemablech.models import CampaignWin

    date              = datetime.now()
    winning_campaigns = defaultdict(set)

    candidates = CampaignWin.objects.filter(
        Q(verified           = True       ) &
        Q(date__hour         = date.hour  ) &
        Q(date__date         = date.date()) &
        Q(campaign__position = 'drawing'  ))

    for candidate in candidates:
        winning_campaigns[candidate.campaign] |= {candidate.user}

    for campaign, users in winning_campaigns.items():
        if len(users):
            winner = users[randrange (0, len(users))]

            Win.objects.create(
                date   = date,
                user   = winner,
                amount = campaign.draw_amount)


def campaign_check(*args, **kwargs):
    from kleemablech.models import Win
    from kleemablech.models import Campaign
    from kleemablech.models import CampaignWin

    date              = datetime.now()
    winning_campaigns = defaultdict(set)

    expired_campaigns = list(filter(lambda _: (
        (_.start_date + timedelta(days=_.duration) < date.date()) or
        (_.campaignwin_set.count() >= _.total_winners           )
    ), Campaign.objects.filter(
        Q(active   = True    ) &
        Q(position = 'slider'))))

    candidates = CampaignWin.objects.filter(
        Q(verified         = True                            ) &
        Q(campaign__id__in = map(lambda _: _.id, expired_campaigns)))

    for candidate in candidates:
        winning_campaigns[candidate.campaign] |= {candidate.user}

    for campaign, users in winning_campaigns.items():
        if len(users):
            winner = list(users)[randrange (0, len(users))]

            Win.objects.create(
                date   = date,
                user   = winner,
                amount = campaign.campaign_end_draw_amount)

    for expired_campaign in expired_campaigns:
        expired_campaign.active = False
        expired_campaign.save()
