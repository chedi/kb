from django.db.models import Q


def refill(self, number, amount, verification_code):
    from gsmmodem.modem     import GsmModem
    from kleemablech.models import Refill
    from kleemablech.models import Operator

    refill = Refill.objects.filter(
        Q(verified          = False            ) &
        Q(verification_code = verification_code)
    ).first()

    if refill:
        for operator in Operator.objects.all():
            if number[0] in operator.prefixes.split(','):
                port           = '/dev/{}'.format(operator.device)
                ussd           = operator.ussd_string % {'number': number, 'amount': amount}
                baud_rate      = operator.baud_rate
                requires_reply = operator.requires_reply
                break

        modem = GsmModem(port, baud_rate)
        modem.connect()
        modem.waitForNetworkCoverage(10)
        print('Sending USSD string to device {}: {}'.format(port, ussd))

        response = modem.sendUssd(ussd)
        print('USSD reply received: {}'.format(response.message))

        if response.sessionActive:
            if requires_reply:
                response.reply('1')
            print('Closing USSD session.')
        refill.verified = True
        refill.save()
        modem.close()
