from os     import environ
from celery import Celery

from django.apps import AppConfig
from django.conf import settings

from kleemablech.taskapp.draws  import hourly_draw
from kleemablech.taskapp.draws  import campaign_check
from kleemablech.taskapp.refill import refill


if not settings.configured:
    environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

app = Celery('kleemablech')


class CeleryConfig(AppConfig):
    name         = 'kleemablech.taskapp'
    verbose_name = 'Celery Config'

    def ready(self):
        app.config_from_object('django.conf:settings')
        app.autodiscover_tasks(lambda: settings.INSTALLED_APPS, force=True)


refill_task         = app.task(refill        , name='refill'        , serializer='json', bind=True)
hourly_draw_task    = app.task(hourly_draw   , name='hourly_draw'   , serializer='json', bind=True)
campaign_check_task = app.task(campaign_check, name='campaign_check', serializer='json', bind=True)
