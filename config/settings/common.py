import environ

from celery.schedules         import crontab
from easy_thumbnails.conf     import Settings as thumbnail_settings
from django.utils.translation import ugettext_lazy as _

env      = environ.Env()
ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('kleemablech')


# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.flatpages',
    'django.contrib.staticfiles',

    # Admin
    'suit',
    'django.contrib.admin',
)
THIRD_PARTY_APPS = (
    'crispy_forms',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'widget_tweaks',
    'snowpenguin.django.recaptcha2',
    'constance',
    'invitations',
    'easy_thumbnails',
    'image_cropping',
    'django_social_share',
    'suit_redactor',
    'reversion',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'kleemablech',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES = (
    # Make sure djangosecure.middleware.SecurityMiddleware is listed first
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool("DJANGO_DEBUG", False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(APPS_DIR.path('../fixtures')),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ("""Nacef LABIDI""", 'chedi.toueiti@gmail.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS      = ADMINS
CONTACT_EMAIL = ['noreply@dev.kleemablech.com', ]

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
    'default': env.db("DATABASE_URL", default="postgres:///kleemablech"),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True


# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
SITE_ID = 1

TIME_ZONE = 'Africa/Tunis'

USE_TZ        = True
USE_I18N      = True
USE_L10N      = True

gettext               = lambda s: s
LANGUAGE_CODE         = 'fr'
TRANSLATION_LANGUAGES = ['fr', 'en']

LANGUAGES = (
    ('fr', gettext('French' )),
    ('en', gettext('English')))

LOCALE_PATHS = (
    str(APPS_DIR.path('../locale')),
)


# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                # Your stuff: custom template context processors go here
                'constance.context_processors.config',
            ],
        },
    },
]

# See: http://django-crispy-forms.readthedocs.org/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(APPS_DIR.path('static')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR.path('../media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Some really nice defaults
ACCOUNT_EMAIL_REQUIRED        = True
ACCOUNT_EMAIL_VERIFICATION    = 'mandatory'
ACCOUNT_AUTHENTICATION_METHOD = 'username'

ACCOUNT_ADAPTER            = 'invitations.models.InvitationsAdapter'
SOCIALACCOUNT_ADAPTER      = 'kleemablech.adapters.SocialAccountAdapter'
ACCOUNT_ALLOW_REGISTRATION = env.bool("DJANGO_ACCOUNT_ALLOW_REGISTRATION", True)

# Custom user app defaults
# Select the correct user model
LOGIN_URL                                             = 'account_login'
AUTH_USER_MODEL                                       = 'kleemablech.User'
LOGIN_REDIRECT_URL                                    = 'home'
ACCOUNT_LOGOUT_ON_GET                                 = True
SOCIALACCOUNT_AUTO_SIGNUP                             = False
ACCOUNT_SIGNUP_FORM_CLASS                             = 'kleemablech.forms.auth.signup.SignupForm'
ACCOUNT_USERNAME_REQUIRED                             = False
ACCOUNT_CONFIRM_EMAIL_ON_GET                          = True
ACCOUNT_AUTHENTICATION_METHOD                         = 'email'
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION                   = True
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = '/signup_step2/'

SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'METHOD': 'oauth2',
        'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
        'SCOPE': ['email', 'public_profile', 'user_friends', 'user_about_me',
                  'user_birthday', 'user_friends', 'user_location'],
        'FIELDS': [
            'id', 'email', 'name', 'first_name', 'last_name', 'verified',
            'locale', 'timezone', 'link', 'gender', 'birthday', 'location', 'updated_time'],
        'EXCHANGE_TOKEN': True,
        'LOCALE_FUNC': 'path.to.callable',
        'VERIFIED_EMAIL': False,
        'VERSION': 'v2.4'
    },
    'google': {
        'SCOPE': ['profile', 'email',
                  'https://www.googleapis.com/auth/plus.me',
                  'https://www.googleapis.com/auth/plus.login',
                  'https://www.googleapis.com/auth/user.birthday.read',
                  'https://www.googleapis.com/auth/user.phonenumbers.read'],
        'AUTH_PARAMS': {'access_type': 'online'}
    }
}

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'

# CELERY
INSTALLED_APPS += ('kleemablech.taskapp.celery.CeleryConfig',)
# if you are not using the django database broker (e.g. rabbitmq, redis, memcached), you can remove the next line.
INSTALLED_APPS += ('kombu.transport.django',)
BROKER_URL = env("CELERY_BROKER_URL", default='redis://localhost:6379/0')

CELERY_ACCEPT_CONTENT = ['json', 'msgpack', 'yaml']

CELERYBEAT_SCHEDULE = {
    'hourly_draw': {
        'task'    : 'hourly_draw',
        'schedule': crontab(minute='0', hour='*'),
    },
    'daily_campaign_check': {
        'task'    : 'daily_campaign_check',
        'schedule': crontab(minute='0', hour='0'),
    }
}

CELERY_TIMEZONE = 'UTC'

# END CELERY

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = r'^admin/'

# Your common stuff: Below this line define 3rd party library settings
NOCAPTCHA             = True
RECAPTCHA_PUBLIC_KEY  = '6LdYox0TAAAAAE_HnpwjVVUSR6r-3XaJw8YVWys7'
RECAPTCHA_PRIVATE_KEY = '6LdYox0TAAAAAHnfIozWW5uNy8tWC5QQANA6DG5-'

CONSTANCE_CONFIG = {
    'SPONSORSHIP_LIMIT'          : (5                                                                          , "Nombre parrainages autorisés par période"   ),
    'SPONSORSHIP_AMOUNT'         : (0.05                                                                       , "Montant du gain par parrainage confirmé"    ),
    'SPONSORSHIP_PERIODE'        : (30                                                                         , "Période de limites de parrainages"          ),
    'SPONSORSHIP_THRESHOLD'      : (0.5                                                                        , "Solde limite des parrainages"               ),
    'SOCIAL_SHARING_GOOGLE'      : ('https://plus.google.com/b/108906763928201279297/108906763928201279297'    , _("Google page link"                        )),
    'SOCIAL_SHARING_YOUTUBE'     : ('https://plus.google.com/u/0/b/108906763928201279297/108906763928201279297', _("Youtube page link"                       )),
    'SOCIAL_SHARING_TWITTER'     : ('https://twitter.com/kleemablech_tn'                                       , _("Twitter page link"                       )),
    'SOCIAL_SHARING_FACEBOOK'    : ('https://www.facebook.com/Kb-Community-1035123963250291/'                  , _("Facebok page link"                       )),
    'TWITTER_CONSUMER_KEY'       : ('AA5q7u66b03ToHYhvV4NDXLuk'                                                , _('Twitter consumer key'                    )),
    'TWITTER_CONSUMER_SECRET'    : ('E0ZL8NaxvFcyusVw5WTISRtCMipFHIMUueQZK42SqXZI3Vs2uj'                       , _('Twitter consumer secret'                 )),
    'TWITTER_ACCESS_TOKEN'       : ('755144231900045312-M7Tq3LU2KH0bajnhMHEGssaydkwYCUz'                       , _('Twitter access token'                    )),
    'TWITTER_ACCESS_TOKEN_SECRET': ('qVYrBXtIueK0JaXs6OUrpRTeKuNqbwe3wPEvrNJgJeHkb'                            , _('Twitter access token secret'             )),
    'FACEBOOK_PAGE_ID'           : ('1035123963250291'                                                         , _('Facebook page id'                        )),
    'FACEBOOK_APP_SECRET'        : ('d28e9c15d632b98ed91dcd7d9558ac41'                                         , _('Facebook app secret'                     )),
    'FACEBOOK_APP_ID'            : ('1733221346961195'                                                         , _('Facebook app id'                         )),
    'PUSHER_KEY'                 : ('d92362c7c0332a0b4213'                                                     , _('Pusher key'                              )),
    'PUSHER_SECRET'              : ('4f9eb0afa5df0ca962a9'                                                     , _('Pusher secret'                           )),
    'PUSHER_APP_ID'              : ('227365'                                                                   , _('Pusher app id'                           )),
    'MAX_DAYLY_SHARES'           : (1                                                                          , _('Max allowed daily shares'                )),
    'MINIMAL_SHARE_CREDIT'       : (0.5                                                                        , _('Minimum credit to be allowed to share'   )),
}

THUMBNAIL_PROCESSORS = (
    'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS

HEADLESS                  = True
THUMBNAIL_DEBUG           = True
IMAGE_CROPPING_THUMB_SIZE = (255, 255)
IMAGE_CROPPING_JQUERY_URL = 'js/vendor/jquery-1.11.1.min.js'


suit_account_models = [
    'kleemablech.user',
    'kleemablech.phonenumber',
    'account.emailaddress',
    'account.emailconfirmation',
]

suit_campagins_models = [
    'kleemablech.campaign',
    'kleemablech.campaignview',
    'kleemablech.win',
    'kleemablech.campaignwin',
    'kleemablech.sponsorshipwin',
    'kleemablech.refill',
]

suit_faq_models = [
    'kleemablech.topic',
    'kleemablech.question',
    'kleemablech.featuredtopic',
]

suit_newsletter_models = [
    'kleemablech.newsletteremails',
]

suit_shares_models = [
    'kleemablech.facebookshare',
    'kleemablech.userfacebookshare',
]

suit_operator_models = [
    'kleemablech.operator',
]

SUIT_CONFIG = {
    'VERSION'                : "0.1",
    'ADMIN_NAME'             : _('Kleemablech'),
    'LIST_PER_PAGE'          : 30,
    'HEADER_DATE_FORMAT'     : 'l, j. F Y',
    'HEADER_TIME_FORMAT'     : 'H:i',
    'MENU_OPEN_FIRST_CHILD'  : True,
    'SHOW_REQUIRED_ASTERISK' : True,
    'CONFIRM_UNSAVED_CHANGES': True,

    'MENU': (
        '-',
        {'app': 'sites'         , 'icon': 'icon-globe'        , },
        {'app': 'flatpages'     , 'icon': 'icon-file'         , },
        {'app': 'constance'     , 'icon': 'icon-wrench'       , },
        {'app': 'invitations'   , 'icon': 'icon-gift'         , },
        {'app': 'socialaccount' , 'icon': 'icon-thumbs-up'    , },
        '-',
        {'label': _('Accounts'    ), 'icon': 'icon-user'         , 'models': suit_account_models   },
        {'label': _('Campaigns'   ), 'icon': 'icon-eye-open'     , 'models': suit_campagins_models },
        '-',
        {'label': _('News letters'), 'icon': 'icon-envelope'     , 'models': suit_newsletter_models},
        {'label': _('F.A.Q'       ), 'icon': 'icon-question-sign', 'models': suit_faq_models       },
        '-',
        {'label': _('Shares'      ), 'icon': 'icon-heart'        , 'models': suit_shares_models    },
        '-',
        {'label': _('Operators'   ), 'icon': 'icon-signal'       , 'models': suit_operator_models  },
    ),
}
