from django.conf                    import settings
from django.views                   import defaults as default_views
from django.contrib                 import admin
from django.conf.urls               import include, url
from django.views.i18n              import javascript_catalog
from django.conf.urls.static        import static
from django.contrib.flatpages.views import flatpage

from kleemablech.views import faq_view
from kleemablech.views import home_view
from kleemablech.views import share_view
from kleemablech.views import history_view
from kleemablech.views import contact_view
from kleemablech.views import transfert_view
from kleemablech.views import dashboard_view
from kleemablech.views import parameters_view
from kleemablech.views import sponsorship_view
from kleemablech.views import step2_signup_view
from kleemablech.views import video_player_view
from kleemablech.views import video_timeout_view
from kleemablech.views import phone_numbers_view
from kleemablech.views import video_counter_view
from kleemablech.views import picture_upload_view
from kleemablech.views import check_phone_number_view
from kleemablech.views import news_letter_emails_view
from kleemablech.views import video_felicitation_view
from kleemablech.views import video_informations_view
from kleemablech.views import monthly_wins_stats_view
from kleemablech.views import monthly_views_stats_view
from kleemablech.views import monthly_refills_stats_view
from kleemablech.views import twitter_followers_count_view
from kleemablech.views import facebook_followers_count_view

js_info_dict = {
    'packages': ('kleemablech.front',),
}

urlpatterns = [
    # User signup
    url(r'^signup_step2/$', step2_signup_view, name='signup_step2'),

    # 3rd parties urls
    url(r'^admin/'      , include(admin.site.urls                            )),
    url(r'^pages/'      , include('django.contrib.flatpages.urls'            )),
    url(r'^accounts/'   , include('allauth.urls'                             )),
    url(r'^invitations/', include('invitations.urls', namespace='invitations')),

    # Profile tabs
    url(r'^history/$'   , history_view   , name='history'   ),
    url(r'^dashboard/$' , dashboard_view , name='dashboard' ),
    url(r'^transfert/$' , transfert_view , name='transfert' ),
    url(r'^parameters/$', parameters_view, name='parameters'),

    # Web services
    url(r'^phone_numbers/$'                 , phone_numbers_view           , name='phone_numbers'           ),
    url(r'^picture_upload/$'                , picture_upload_view          , name='picture_upload'          ),
    url(r'^news_letter_emails/$'            , news_letter_emails_view      , name='news_letter_emails'      ),
    url(r'^check_phone_number/(?P<phone>)'  , check_phone_number_view      , name='check_phone_number'      ),
    url(r'^twitter_followers_count_view/$'  , twitter_followers_count_view , name='twitter_followers_count' ),
    url(r'^facebook_followers_count_view/$' , facebook_followers_count_view, name='facebook_followers_count'),

    # Main pages
    url(r'^$'            , home_view       , name="home"       ),
    url(r'^faq/$'        , faq_view        , name="faq"        ),
    url(r'^share/$'      , share_view      , name="share"      ),
    url(r'^contact/$'    , contact_view    , name="contact"    ),
    url(r'^sponsorship/$', sponsorship_view, name="sponsorship"),

    # Flat pages
    url(r'^privacy_policy/$'      , flatpage, {'url': '/privacy_policy/'      }, name='privacy_policy'      ),
    url(r'^terms_and_conditions/$', flatpage, {'url': '/terms_and_conditions/'}, name='terms_and_conditions'),

    # Videos
    url(r'^video_timeout/$'                       , video_timeout_view     , name='video_timeout'     ),
    url(r'^video_player/(?P<video>[0-9]+)/$'      , video_player_view      , name='video_player'      ),
    url(r'^video_counter/(?P<video>[0-9]+)/$'     , video_counter_view     , name='video_counter'     ),
    url(r'^video_felicitation/(?P<key>.+)/$'      , video_felicitation_view, name='video_felicitation'),
    url(r'^video_informations/(?P<video>[0-9]+)/$', video_informations_view, name='video_informations'),

    # Statistics
    url(r'^stats/win/monthly/$'   , monthly_wins_stats_view   , name='monthly_wins_stats'   ),
    url(r'^stats/view/monthly/$'  , monthly_views_stats_view  , name='monthly_views_stats'  ),
    url(r'^stats/refill/monthly/$', monthly_refills_stats_view, name='monthly_refills_stats'),

    # Javascript localization
    url(r'^jsi18n/$', javascript_catalog, js_info_dict, name='javascript-catalog'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += [
        url(r'^500/$', default_views.server_error                                                           ),
        url(r'^400/$', default_views.bad_request      , kwargs={'exception': Exception("Bad Request!"     )}),
        url(r'^404/$', default_views.page_not_found   , kwargs={'exception': Exception("Page not Found"   )}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception("Permission Denied")}),
    ]
